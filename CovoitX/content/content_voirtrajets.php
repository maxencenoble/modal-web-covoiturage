<?php

//1er Tableau------------------------------------------------------------------------------------
echo"<div class='card text-center'>";
echo "<h5 class='card-header'> Trajets proposés par les utilisateurs du site</h5>";
echo "<div class='card-body'>";
$data=TrajetPropose::getAllVoyagePro($dbh);
?>

<table id="voirtrajets1" class="display">
    <thead>
        <tr>
            <th>Date</th>
            
            <th>Départ</th>
            <th>Arrivée</th>
            <th>Etapes possibles</th>
            <th></th>
        </tr>
    </thead>
    <tbody id="body1">
<?php


foreach ($data as $trajet){
    $email=$trajet->conducteur;
    $etapes= TrajetPropose::getAllEtapes($dbh,$trajet);
    $liste_etapes="";
    foreach ($etapes as $lieuetape){
        $lieuetape=$lieuetape[0];
        $liste_etapes=$liste_etapes.$lieuetape.", ";
    }
    $user= Utilisateur::getUtilisateur($dbh, $email);
    $prenom=$user->prenom;
    $nom=$user->nom;
    $contact=$user->contact;
    $lat_dep=$trajet->latitude_dep;
    $long_dep=$trajet->longitude_dep;
    $lat_arr=$trajet->latitude_arr;
    $long_arr=$trajet->longitude_arr;
    $places_pro=$trajet->places;
    $info_supp_pro=$trajet->infosupp;
    $voyagepro=$trajet->voyagepro;
    $datejours= TrajetPropose::tous_les_pro($trajet);
    if (!TrajetPropose::estAccepte($dbh,$email,$voyagepro)){
    echo <<<FIN
    <tr>
        <td>$datejours</td>
        
        <td>$trajet->depart à $trajet->debut</td>
        <td>$trajet->arrivee à $trajet->fin</td>
        <td>$liste_etapes</td>
        <td><button type="button"  id="$lat_dep;$long_dep;$lat_arr;$long_arr;$places_pro;$info_supp_pro;$prenom;$nom;$contact;$voyagepro" class ="btn-ensavoirplus_pro btn btn-warning"> En savoir plus</button></td>
    </tr>
    FIN;}
}
?>
    </tbody>
</table>


<?php
//Endroit ou sera généré dynamiquement "en savoir plus"
echo <<<FIN

<div class="container" id ="carte_voir_pro">
<br>
<h1 class='center'> <span style="text-decoration:underline">Détails supplémentaires</span></h1>   
<br>
    <div class="row">
    <div  class="col-sm card" style="text-align:center">
      <span class="centre-souligne"> 1. Point de départ proposé</span>
    </div>
    <div style="text-align:center" class="col-sm card">
      <span class="centre-souligne"> 2. Point d'arrivée proposé</span>
    </div>
    <div style="text-align:center" class="col-sm card">
      <span class="centre-souligne"> 3. Informations sur le trajet</span>
    </div>
    </div>

<div class="row justify-content-arounds">
<div style="height:350px" class="card col-sm" id="map-canvas_voir_pro_dep"></div>
<div style="height:350px" class="card col-sm" id="map-canvas_voir_pro_arr"></div>

<div class="col-sm card " id="detail_pro">
<dt >Conducteur : </dt>
<dd id="conducteur"></dd>

<dt >Contact : </dt>
<dd id="contact_pro"></dd>

<dt >Places disponibles : </dt>
<dd id="places_pro"></dd>

<dt >Infos supplémentaires sur le trajet : </dt>
<dd id="info_supp_pro"></dd>

<br>
<div id='bouton_pro'>

</div>
</div>

</div>

</div>
        
</div>
</div>
FIN
?>


<?php
//2e Tableau--------------------------------------------------------------------------------------------------
echo"<div class='card text-center'>";
echo "<div class='card-header'><h5>Trajets recherchés par les utilisateurs du site </h5><h7>(le niveau de jaune indique l'urgence de la demande)</h7></div>";
echo "<div class='card-body'>";
$databis= TrajetRecherche::getAllVoyageRec($dbh);
?>

<table id="voirtrajets2" class="display">
    <thead>
        <tr>
            <th>Date</th>
            
            <th>Départ</th>
            <th>Arrivée</th>
            
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php



foreach ($databis as $trajet){
    $email=$trajet->demandeur;
    $user= Utilisateur::getUtilisateur($dbh, $email);
    $prenom=$user->prenom;
    $nom=$user->nom;
    $contact=$user->contact;
    $lat_dep=$trajet->latitude_dep;
    $long_dep=$trajet->longitude_dep;
    $lat_arr=$trajet->latitude_arr;
    $long_arr=$trajet->longitude_arr;
    $categorie=$trajet->categorie;
    $info_supp_rec=$trajet->infosupp;
    $voyagerec=$trajet->voyagerec;
    $datejours= TrajetRecherche::tous_les_rec($trajet);
    $couleururgence=TrajetRecherche::couleururgence($trajet);
    if (!TrajetRecherche::estAccepte($dbh,$email,$voyagerec)){
    echo <<<FIN
    <tr style="background-color:$couleururgence">
        <td>$datejours</td>
        
        <td>$trajet->depart à $trajet->debut</td>
        <td>$trajet->arrivee à $trajet->fin</td>
        
        <td><button type="button"  id="$lat_dep;$long_dep;$lat_arr;$long_arr;$categorie;$info_supp_pro;$prenom;$nom;$contact;$voyagerec" class ="btn-ensavoirplus_rec btn btn-warning"> En savoir plus</button></td>

    </tr>
    FIN;}
}
?>
    </tbody>
</table>

<?php
//Endroit ou sera généré dynamiquement "en savoir plus"
echo <<<FIN

<div class="container" id ="carte_voir_rec">
<br>
<h1 class='center'> <span style="text-decoration:underline">Détails supplémentaires</span></h1>   
<br>
    <div class="row">
    <div class="col-sm card" style="text-align:center">
      <span class="centre-souligne"> 1. Point de départ proposé</span>
    </div>
    <div class="col-sm card" style="text-align:center">
      <span class="centre-souligne"> 2. Point d'arrivée proposé</span>
    </div>
    <div class="col-sm card" style="text-align:center">
      <span class="centre-souligne"> 3. Informations sur le trajet</span>
    </div>
    </div>

<div class="row justify-content-arounds">
<div style="height:350px" class="card col-sm" id="map-canvas_voir_rec_dep"></div>
<div style="height:350px" class="card col-sm" id="map-canvas_voir_rec_arr"></div>
<div class="col-sm card " id="detail_rec">
<dt >Demandeur : </dt>
<dd id="demandeur"></dd>

<dt >Contact : </dt>
<dd id="contact_rec"></dd>

<dt >Catégorie : </dt>
<dd id="categorie"></dd>

<dt >Infos supplémentaires sur le trajet : </dt>
<dd id="info_supp_rec"></dd>

<br>
<div id="bouton_rec"> </div>

</div>
</div>
</div>
</div>
</div> 
FIN
?>
