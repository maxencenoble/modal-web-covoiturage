<?php
if (isset($_POST["id_voyage"]) && $_POST["id_voyage"] != "" &&
        isset($_POST["client"]) && $_POST["client"] != "") {
    $id_voyage = $_POST["id_voyage"];
    $client = $_POST["client"];
    if ($client == "propose") {
        echo <<<FIN
        <div class='card text-center'>
        <h5 class='card-header'> Votre trajet </h5>
        <div class='card-body'>
        FIN;
        TrajetPropose::afficherPersonal($dbh,$id_voyage);
        echo "</div>";
        echo "</div>";
        echo "<br>";
        echo"<div class='card text-center'>";
        echo "<h5 class='card-header'> Trajets recherchés par les utilisateurs du site correspondant le plus à vos critères </h5>";
        echo "<div class='card-body'>";
        $databis = TrajetPropose::getAllMatching1($dbh, $id_voyage);
        $databis2=TrajetPropose::getAllMatching2($dbh, $id_voyage);
        if ($databis != NULL) {
            ?>

            <table id="voirtrajets2" class="display">
                <thead>
                    <tr>
                        <th>Date</th>
                        
                        <th>Départ</th>
                        <th>Arrivée</th>
                        
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    
                    foreach ($databis as $trajet) {
                        
                        $email = $trajet->demandeur;
                        $user = Utilisateur::getUtilisateur($dbh, $email);
                        $prenom = $user->prenom;
                        $nom = $user->nom;
                        $contact = $user->contact;
                        $lat_dep = $trajet->latitude_dep;
                        $long_dep = $trajet->longitude_dep;
                        $lat_arr = $trajet->latitude_arr;
                        $long_arr = $trajet->longitude_arr;
                        $categorie = $trajet->categorie;
                        $info_supp_rec = $trajet->infosupp;
                        $voyagerec = $trajet->voyagerec;
                        $datejours = TrajetRecherche::tous_les_rec($trajet);
                        if (!TrajetRecherche::estAccepte($dbh, $email, $voyagerec)) {
                            echo <<<FIN
                            <tr>
                                <td>$datejours</td>
                                
                                <td>$trajet->depart à $trajet->debut</td>
                                <td>$trajet->arrivee à $trajet->fin</td>
                                
                                <td><button type="button"  id="$lat_dep;$long_dep;$lat_arr;$long_arr;$categorie;$info_supp_rec;$prenom;$nom;$contact;$voyagerec" class ="btn-ensavoirplus_rec btn btn-warning"> En savoir plus</button></td>
                            </tr>
                            FIN;
                        }
                    }
                    
                        ?>
                    </tbody>
            </table>
            <?php
            echo '<button type="button"  id="btn-matching2_non_null" class =" btn btn-warning"> Etendre vos critères</button>';
            echo "</div>";
            echo "</div>";
            ?>
                    <div class='matching2_non_null text-center'>
                    <?php
                    echo "<br>";
                    echo"<div class='card text-center'>";
                    echo "<h5 class='card-header'> Critères moins respectés</h5>";
                    echo "<div class='card-body'>";
                    if($databis2!=NULL){
                    ?>
                    <table id="voirtrajets3" class="display">
                        <thead>
                            <tr>
                                <th>Date</th>
                                
                                <th>Départ</th>
                                <th>Arrivée</th>
                                
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php

                        foreach ($databis2 as $trajet) {

                            $email = $trajet->demandeur;
                            $user = Utilisateur::getUtilisateur($dbh, $email);
                            $prenom = $user->prenom;
                            $nom = $user->nom;
                            $contact = $user->contact;
                            $lat_dep = $trajet->latitude_dep;
                            $long_dep = $trajet->longitude_dep;
                            $lat_arr = $trajet->latitude_arr;
                            $long_arr = $trajet->longitude_arr;
                            $categorie = $trajet->categorie;
                            $info_supp_rec = $trajet->infosupp;
                            $voyagerec = $trajet->voyagerec;
                            $datejours = TrajetRecherche::tous_les_rec($trajet);
                            if (!TrajetRecherche::estAccepte($dbh, $email, $voyagerec)) {
                                echo <<<FIN
                                <tr>
                                    <td>$datejours</td>
                                    
                                    <td>$trajet->depart à $trajet->debut</td>
                                    <td>$trajet->arrivee à $trajet->fin</td>
                                    
                                    <td><button type="button"  id="$lat_dep;$long_dep;$lat_arr;$long_arr;$categorie;$info_supp_rec;$prenom;$nom;$contact;$voyagerec" class ="btn-ensavoirplus_rec btn btn-warning"> En savoir plus</button></td>
                                </tr>
                                FIN;
                            }
                        }




                        ?>
                        </tbody>
                    </table>
                    <?php
                    
                    echo "<br>";
                    echo "Si vous n'êtes pas satisfait essayez de voir sur l'ensemble des trajets !";
                    echo "<br>";
                    echo "<br>";
                    echo '<a href="index.php?page=voirtrajets" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Retrouver tous les trajets recherchés ici.</a>';
                    echo "</div>";
                    echo "</div>";
                    
                    } else{
                        echo "Pas d'autres trajets, essayez de voir tous les trajets !";
                        echo "<br>";
                        echo "<br>";
                        echo '<a href="index.php?page=voirtrajets" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Retrouver tous les trajets recherchés ici.</a>';
                        echo "</div>";
                        echo "</div>";
                    }
                    ?>
                    </div>
            <?php
            
            
            
            
            echo <<<FIN
            <div class="container" id ="carte_voir_rec">
            <br>
            <h1 align='center'> <span style="text-decoration:underline">Détails supplémentaires</span></h1>   
            <br>
                <div class="row">
                <div style="text-align:center" class="col-sm card">
                  <span style="text-decoration:underline"> 1. Point de départ proposé</span>
                </div>
                <div style="text-align:center" class="col-sm card">
                  <span style="text-decoration:underline"> 2. Point d'arrivée proposé</span>
                </div>
                <div style="text-align:center" class="col-sm card">
                  <span style="text-decoration:underline"> 3. Informations sur le trajet</span>
                </div>
                </div>
            <div class="row justify-content-arounds">
            <div style="height:350px" class="card col-sm" id="map-canvas_voir_rec_dep"></div>
            <div style="height:350px" class="card col-sm" id="map-canvas_voir_rec_arr"></div>
            <div class="col-sm card text-center" id="detail_rec">
            <dt >Demandeur : </dt>
            <dd id="demandeur"></dd>
            <dt >Contact : </dt>
            <dd id="contact_rec"></dd>
            <dt >Catégorie : </dt>
            <dd id="categorie"></dd>
            <dt >Infos supplémentaires sur le trajet : </dt>
            <dd id="info_supp_rec"></dd>
            <br>
            <div id="bouton_rec">
            </div>
            </div>
            </div>
            </div>
            
            FIN;
            
            
            echo "</div>";
            echo "</div>";
        } else {
            echo "Pas de voyage correspondant à vos premiers critères,essayez d'étendre vos critères !";
            echo "<br>";
            echo "<br>";
            echo '<button type="button"  id="btn-matching2_null" class =" btn btn-warning"> Etendre vos critères</button>';
            echo "</div>";
            echo "</div>";
            ?>
            <div class='matching2_null text-center'>
                    <?php
                    echo "<br>";
                    echo"<div class='card text-center'>";
                    echo "<h5 class='card-header'> Critères moins respectés</h5>";
                    echo "<div class='card-body'>";
                    if($databis2!=NULL){
                    ?>
                    <table id="voirtrajets2" class="display">
                        <thead>
                            <tr>
                                <th>Date</th>
                                
                                <th>Départ</th>
                                <th>Arrivée</th>
                                
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                    <?php
                    
                    foreach ($databis2 as $trajet) {
                        
                        $email = $trajet->demandeur;
                        $user = Utilisateur::getUtilisateur($dbh, $email);
                        $prenom = $user->prenom;
                        $nom = $user->nom;
                        $contact = $user->contact;
                        $lat_dep = $trajet->latitude_dep;
                        $long_dep = $trajet->longitude_dep;
                        $lat_arr = $trajet->latitude_arr;
                        $long_arr = $trajet->longitude_arr;
                        $categorie = $trajet->categorie;
                        $info_supp_rec = $trajet->infosupp;
                        $voyagerec = $trajet->voyagerec;
                        $datejours = TrajetRecherche::tous_les_rec($trajet);
                        if (!TrajetRecherche::estAccepte($dbh, $email, $voyagerec)) {
                            echo <<<FIN
                            <tr>
                                <td>$datejours</td>
                                
                                <td>$trajet->depart à $trajet->debut</td>
                                <td>$trajet->arrivee à $trajet->fin</td>
                                
                                <td><button type="button"  id="$lat_dep;$long_dep;$lat_arr;$long_arr;$categorie;$info_supp_rec;$prenom;$nom;$contact;$voyagerec" class ="btn-ensavoirplus_rec btn btn-warning"> En savoir plus</button></td>
                            </tr>
                            FIN;
                        }
                    }
                    
                    
                
                    
                    ?>
                        </tbody>
                    </table>
                    <?php
                    echo "</div>";
                    echo "</div>";
                    
                    echo <<<FIN
                    <div class="container" id ="carte_voir_rec">
                    <br>
                    <h1 align='center'> <span style="text-decoration:underline">Détails supplémentaires</span></h1>   
                    <br>
                        <div class="row">
                        <div style="text-align:center" class="col-sm card">
                          <span style="text-decoration:underline"> 1. Point de départ proposé</span>
                        </div>
                        <div style="text-align:center" class="col-sm card">
                          <span style="text-decoration:underline"> 2. Point d'arrivée proposé</span>
                        </div>
                        <div style="text-align:center" class="col-sm card">
                          <span style="text-decoration:underline"> 3. Informations sur le trajet</span>
                        </div>
                        </div>
                    <div class="row justify-content-arounds">
                    <div style="height:350px" class="card col-sm" id="map-canvas_voir_rec_dep"></div>
                    <div style="height:350px" class="card col-sm" id="map-canvas_voir_rec_arr"></div>
                    <div class="col-sm card text-center" id="detail_rec">
                    <dt >Demandeur : </dt>
                    <dd id="demandeur"></dd>
                    <dt >Contact : </dt>
                    <dd id="contact_rec"></dd>
                    <dt >Catégorie : </dt>
                    <dd id="categorie"></dd>
                    <dt >Infos supplémentaires sur le trajet : </dt>
                    <dd id="info_supp_rec"></dd>
                    <br>
                    <div id="bouton_rec">
                    </div>
                    </div>
                    </div>
                    </div>
                    
                    
                    FIN;
                    
                    echo "<br>";
                    echo "Si vous n'êtes pas satisfait essayez de voir sur l'ensemble des trajets !";
                    echo "<br>";
                    echo "<br>";
                    echo '<a href="index.php?page=voirtrajets" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Retrouver tous les trajets recherchés ici.</a>';
                    echo "</div>";
                    echo "</div>";
                    } else{
                        echo "Pas d'autres trajets, essayez de voir tous les trajets !";
                        echo "<br>";
                        echo "<br>";
                        echo '<a href="index.php?page=voirtrajets" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Retrouver tous les trajets recherchés ici.</a>';
                        echo "</div>";
                        echo "</div>";
                    }
                    ?>
                    </div>
                    <?php
            
            
            
            }
            
            }
     else {
        echo <<<FIN
        <div class='card text-center'>
        <h5 class='card-header'> Votre trajet </h5>
        <div class='card-body'>
        FIN;
        TrajetRecherche::afficherPersonal($dbh,$id_voyage);
        echo "</div>";
        echo "</div>";
        echo "<br>";
        echo"<div class='card text-center'>";
        echo "<h5 class='card-header'> Trajets proposés par les utilisateurs du site correspondant le plus à vos critères </h5>";
        echo "<div class='card-body'>";
        $databis = TrajetRecherche::getAllMatching1($dbh, $id_voyage);
        $databis2=TrajetRecherche::getAllMatching2($dbh, $id_voyage);
        if ($databis != NULL) {
            ?>

            <table id="voirtrajets2" class="display">
                <thead>
                    <tr>
                        <th>Date</th>
                        
                        <th>Départ</th>
                        <th>Arrivée</th>
                        
                        <th>Etapes possibles</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    
                    foreach ($databis as $trajet) {
                        
                        $email = $trajet->conducteur;
                        $etapes= TrajetPropose::getAllEtapes($dbh,$trajet);
                        $liste_etapes="";
                        foreach ($etapes as $lieuetape){
                            $lieuetape=$lieuetape[0];
                            $liste_etapes=$liste_etapes.$lieuetape.", ";
                        }
                        $user = Utilisateur::getUtilisateur($dbh, $email);
                        $prenom = $user->prenom;
                        $nom = $user->nom;
                        $contact = $user->contact;
                        $lat_dep = $trajet->latitude_dep;
                        $long_dep = $trajet->longitude_dep;
                        $lat_arr = $trajet->latitude_arr;
                        $long_arr = $trajet->longitude_arr;
                        $places_pro=$trajet->places;
                        $info_supp_pro = $trajet->infosupp;
                        $voyagepro = $trajet->voyagepro;
                        $datejours = TrajetPropose::tous_les_pro($trajet);
                        if (!TrajetPropose::estAccepte($dbh, $email, $voyagepro)) {
                            echo <<<FIN
                            <tr>
                                <td>$datejours</td>
                                
                                <td>$trajet->depart à $trajet->debut</td>
                                <td>$trajet->arrivee à $trajet->fin</td>
                                
                                <td>$liste_etapes</td>
                                <td><button type="button"  id="$lat_dep;$long_dep;$lat_arr;$long_arr;$places_pro;$info_supp_pro;$prenom;$nom;$contact;$voyagepro" class ="btn-ensavoirplus_pro btn btn-warning"> En savoir plus</button></td>
                            </tr>
                            FIN;
                        }
                    }
                    
                        ?>
                    </tbody>
            </table>
            <?php
            echo '<button type="button"  id="btn-matching2_non_null" class =" btn btn-warning"> Etendre vos critères</button>';
            echo "</div>";
            echo "</div>";
            ?>
                    <div class='matching2_non_null text-center'>
                    <?php
                    echo "<br>";
                    echo"<div class='card text-center'>";
                    echo "<h5 class='card-header'> Critères moins respectés</h5>";
                    echo "<div class='card-body'>";
                    if($databis2!=NULL){
                    ?>
                    <table id="voirtrajets3" class="display">
                        <thead>
                            <tr>
                                <th>Date</th>
                                
                                <th>Départ</th>
                                <th>Arrivée</th>
                                
                                <th>Etapes possibles</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php

                        foreach ($databis2 as $trajet) {

                            $email = $trajet->conducteur;
                            $etapes= TrajetPropose::getAllEtapes($dbh,$trajet);
                            $liste_etapes="";
                            foreach ($etapes as $lieuetape){
                                $lieuetape=$lieuetape[0];
                                $liste_etapes=$liste_etapes.$lieuetape.", ";
                            }
                            $user = Utilisateur::getUtilisateur($dbh, $email);
                            $prenom = $user->prenom;
                            $nom = $user->nom;
                            $contact = $user->contact;
                            $lat_dep = $trajet->latitude_dep;
                            $long_dep = $trajet->longitude_dep;
                            $lat_arr = $trajet->latitude_arr;
                            $long_arr = $trajet->longitude_arr;
                            $places_pro=$trajet->places;
                            $info_supp_pro = $trajet->infosupp;
                            $voyagepro = $trajet->voyagepro;
                            $datejours = TrajetRecherche::tous_les_pro($trajet);
                            if (!TrajetPropose::estAccepte($dbh, $email, $voyagepro)) {
                                echo <<<FIN
                                <tr>
                                    <td>$datejours</td>
                                    
                                    <td>$trajet->depart à $trajet->debut</td>
                                    <td>$trajet->arrivee à $trajet->fin</td>
                                    
                                    <td>$liste_etapes</td>
                                    <td><button type="button"  id="$lat_dep;$long_dep;$lat_arr;$long_arr;$places_pro;$info_supp_pro;$prenom;$nom;$contact;$voyagepro" class ="btn-ensavoirplus_pro btn btn-warning"> En savoir plus</button></td>
                                </tr>
                                FIN;
                            }
                        }




                        ?>
                        </tbody>
                    </table>
                    <?php
                    
                    echo "<br>";
                    echo "Si vous n'êtes pas satisfait essayez de voir sur l'ensemble des trajets !";
                    echo "<br>";
                    echo "<br>";
                    echo '<a href="index.php?page=voirtrajets" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Retrouver tous les trajets proposés ici.</a>';
                    echo "</div>";
                    echo "</div>";
                    
                    } else{
                        echo "Pas d'autres trajets, essayez de voir tous les trajets !";
                        echo "<br>";
                        echo "<br>";
                        echo '<a href="index.php?page=voirtrajets" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Retrouver tous les trajets proposés ici.</a>';
                        echo "</div>";
                        echo "</div>";
                    }
                    ?>
                    </div>
            <?php
            
            
            
            
                    echo <<<FIN
                    <div class="container" id ="carte_voir_pro">
                    <br>
                    <h1 align='center'> <span style="text-decoration:underline">Détails supplémentaires</span></h1>   
                    <br>
                        <div class="row">
                        <div style="text-align:center" class="col-sm card">
                          <span style="text-decoration:underline"> 1. Point de départ proposé</span>
                        </div>
                        <div style="text-align:center" class="col-sm card">
                          <span style="text-decoration:underline"> 2. Point d'arrivée proposé</span>
                        </div>
                        <div style="text-align:center" class="col-sm card">
                          <span style="text-decoration:underline"> 3. Informations sur le trajet</span>
                        </div>
                        </div>
                    <div class="row justify-content-arounds">
                    <div style="height:350px" class="card col-sm" id="map-canvas_voir_pro_dep"></div>
                    <div style="height:350px" class="card col-sm" id="map-canvas_voir_pro_arr"></div>
                    <div class="col-sm card text-center" id="detail_pro">
                    <dt >Conducteur : </dt>
                    <dd id="conducteur"></dd>
                    <dt >Contact : </dt>
                    <dd id="contact_pro"></dd>
                    <dt >Places disponibles : </dt>
                    <dd id="places_pro"></dd>
                    <dt >Infos supplémentaires sur le trajet : </dt>
                    <dd id="info_supp_pro"></dd>
                    <br>
                    <div id="bouton_pro">
                    </div>
                    </div>
                    </div>
                    </div>

                    FIN;
            
            
            echo "</div>";
            echo "</div>";
        } else {
            echo "Pas de voyage correspondant à vos premiers critères,essayez d'étendre vos critères !";
            echo "<br>";
            echo "<br>";
            echo '<button type="button"  id="btn-matching2_null" class =" btn btn-warning"> Etendre vos critères</button>';
            echo "</div>";
            echo "</div>";
            ?>
            <div class='matching2_null text-center'>
                    <?php
                    echo "<br>";
                    echo"<div class='card text-center'>";
                    echo "<h5 class='card-header'> Critères moins respectés</h5>";
                    echo "<div class='card-body'>";
                    if($databis2!=NULL){
                    ?>
                    <table id="voirtrajets2" class="display">
                        <thead>
                            <tr>
                                <th>Date</th>
                                
                                <th>Départ</th>
                                <th>Arrivée</th>
                                
                                <th>Etapes possibles</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                    <?php
                    
                    foreach ($databis2 as $trajet) {
                        
                        $email = $trajet->conducteur;
                        $etapes= TrajetPropose::getAllEtapes($dbh,$trajet);
                        $liste_etapes="";
                        foreach ($etapes as $lieuetape){
                            $lieuetape=$lieuetape[0];
                            $liste_etapes=$liste_etapes.$lieuetape.", ";
                        }
                        $user = Utilisateur::getUtilisateur($dbh, $email);
                        $prenom = $user->prenom;
                        $nom = $user->nom;
                        $contact = $user->contact;
                        $lat_dep = $trajet->latitude_dep;
                        $long_dep = $trajet->longitude_dep;
                        $lat_arr = $trajet->latitude_arr;
                        $long_arr = $trajet->longitude_arr;
                        $places_pro=$trajet->places;
                        $info_supp_pro=$trajet->infosupp;
                        $voyagepro = $trajet->voyagepro;
                        $datejours = TrajetPropose::tous_les_pro($trajet);
                        if (!TrajetPropose::estAccepte($dbh, $email, $voyagepro)) {
                            echo <<<FIN
                            <tr>
                                <td>$datejours</td>
                                
                                <td>$trajet->depart à $trajet->debut</td>
                                <td>$trajet->arrivee à $trajet->fin</td>
                                
                                <td>$liste_etapes</td>
                                <td><button type="button"  id="$lat_dep;$long_dep;$lat_arr;$long_arr;$places_pro;$info_supp_pro;$prenom;$nom;$contact;$voyagepro" class ="btn-ensavoirplus_pro btn btn-warning"> En savoir plus</button></td>
                            </tr>
                            FIN;
                        }
                    }
                    
                    
                
                    
                    ?>
                        </tbody>
                    </table>
                    <?php
                    echo "</div>";
                    echo "</div>";
                    
                    echo <<<FIN
                    <div class="container" id ="carte_voir_pro">
                    <br>
                    <h1 align='center'> <span style="text-decoration:underline">Détails supplémentaires</span></h1>   
                    <br>
                        <div class="row">
                        <div style="text-align:center" class="col-sm card">
                          <span style="text-decoration:underline"> 1. Point de départ proposé</span>
                        </div>
                        <div style="text-align:center" class="col-sm card">
                          <span style="text-decoration:underline"> 2. Point d'arrivée proposé</span>
                        </div>
                        <div style="text-align:center" class="col-sm card">
                          <span style="text-decoration:underline"> 3. Informations sur le trajet</span>
                        </div>
                        </div>
                    <div class="row justify-content-arounds">
                    <div style="height:350px" class="card col-sm" id="map-canvas_voir_pro_dep"></div>
                    <div style="height:350px" class="card col-sm" id="map-canvas_voir_pro_arr"></div>
                    <div class="col-sm card text-center " id="detail_pro">
                    <dt >Conducteur : </dt>
                    <dd id="conducteur"></dd>
                    <dt >Contact : </dt>
                    <dd id="contact_pro"></dd>
                    <dt >Places disponibles : </dt>
                    <dd id="places_pro"></dd>
                    <dt >Infos supplémentaires sur le trajet : </dt>
                    <dd id="info_supp_pro"></dd>
                    <br>
                    <div id="bouton_pro">
                    </div>
                    </div>
                    </div>
                    </div>
                    
                    
                    FIN;
                    
                    echo "<br>";
                    echo "Si vous n'êtes pas satisfait essayez de voir sur l'ensemble des trajets !";
                    echo "<br>";
                    echo "<br>";
                    echo '<a href="index.php?page=voirtrajets" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Retrouver tous les trajets proposés ici.</a>';
                    echo "</div>";
                    echo "</div>";
                    } else{
                        echo "Pas d'autres trajets, essayez de voir tous les trajets !";
                        echo "<br>";
                        echo "<br>";
                        echo '<a href="index.php?page=voirtrajets" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Retrouver tous les trajets proposés ici.</a>';
                        echo "</div>";
                        echo "</div>";
                    }
                    ?>
            </div>
                    <?php
            
            
            
            }
            
            
    }
}
