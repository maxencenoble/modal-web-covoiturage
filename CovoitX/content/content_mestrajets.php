<?php

$user = Utilisateur::getUtilisateur($dbh, $_SESSION["email"]);


$aAfficherPro = TrajetPropose::getTrajetsProUser($dbh, $user->email);
$aAfficherRec = TrajetRecherche::getTrajetsRecUser($dbh, $user->email);

echo "<div class='card text-center'>";
echo "<div class='card-header'>";
echo <<<FIN
<ul class="nav nav-tabs card-header-tabs justify-content-center">
      <li class="nav-item">
        <a class="nav-link active" href="">Comment ça fonctionne ?</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?page=mestrajetspro">Mettre à jour mes propositions</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?page=mestrajetsrec">Mettre à jour mes recherches</a>
      </li>
</ul>
FIN;

echo <<<FIN
</div>
<div class='card-body'>
<h5 class="card-title">Voilà comment ça se passe</h5>
<br>
<div class="card text-center">
    <h6 class="card-header">1 - Tu peux modifier les infos du voyage </h6>
    <div class="card-body"> Si ton trajet n'est pas déjà accepté, tu peux modifier les informations pratiques de ce trajet (horaire de départ, date, nombre de places disponibles...)</div>
</div>
<br>
<div class="card text-center">
    <h6 class="card-header">2 - Tu peux supprimer ton voyage si tu n'en as plus envie </h6>
    <div class="card-body"> Si ton trajet n'est pas déjà accepté, tu peux le retirer des trajets enregistrés.</div>
</div>
<br>
<div class="card text-center">
    <h6 class="card-header">3 - Si tu as trouvé un trajet qui t'intéresse, mets à jour ton trajet</h6>
    <div class="card-body"> En notant le trajet comme "Accepté", tu le retires de la liste des trajets visibles. Tu peux cependant le réinsérer par la suite s'il y a désistement.</div>
</div>
<br>



</div>
</div>

FIN;



