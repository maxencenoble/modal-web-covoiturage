<?php

echo <<<FIN
<div class="card text-center">
    <h5 class="card-header">Bonjour et Bienvenue sur le site de covoiturage du plateau de Saclay</h5>
    <div class="card-body">
    <div class="card-deck">
    <div class="card text-center">
    <h6 class="card-header"> Pourquoi un site de covoiturage ? </h6>
    <img class="card-img-top" src="https://www.demain.fr/wp-content/uploads/2017/10/Essonne-le-Plateau-de-Saclay-un-po%CC%82le-e%CC%81conomique-en-de%CC%81bat-avec-la-me%CC%81tropole.jpg" alt="Photographie Plateau de Saclay">
    <div class="card-body">
    Ce site designé à l'occasion d'un projet informatique de deux élèves de l'Ecole Polytechnique a pour but de simplifier, centraliser et fluidifier les propositions et demandes de covoiturage sur le plateau.
    <br>
    <br>
    Etant tributaires des moyens de transport collectifs (plus ou moins réguliers), ce site a pour ambition d'offrir une alternative durable et écologique aux personnes qui souhaitent se déplacer à l'intérieur comme à l'extérieur du plateau de Saclay.
    </div>
    </div>
    <div class="card text-center" >
    <h6 class="card-header"> Comment participer ? </h6>
    <div class="card-body">
    Ce site de covoiturage a été conçu pour répondre à une demande croissante d'offres et de demandes de covoiturage : c'est dans une perspective de satisfaire au maximum les utilisateurs que l'interface a été pensée de façon pratique.
    <br>
    <br>
    Si vous souhaitez faire partie de l'aventure en tant que conducteur ou demandeur, inscrivez-vous (ça ne prend que quelques minutes, et c'est gratuit) et suivez les instructions pour pouvoir profiter de toutes les fonctionnalités proposées.
    </div>
    <img class="card-img-bottom" src="https://www.ipj.news/enquetes/wp-content/uploads/sites/26/2019/06/illustration-covoiturage-20170827.jpg" alt="Covoiturage">
    </div>
    </div>
    </div>
</div>
FIN;

