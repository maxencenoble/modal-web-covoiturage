<?php
$form_values_valid = false;

if (isset($_POST["email"]) && $_POST["email"] != "" && isset($_POST["password"]) && $_POST["password"] != "" ){
    $login = $_POST["email"];
    $user = utilisateur :: getUtilisateur($dbh, $login);
    if ($user == NULL) {
        echo "<span class='avertissement'>Pas d'utilisateur à ce nom.</span>";
    } else {
        if (utilisateur::testerMDP($dbh, $user, $_POST['password'])) {
            utilisateur::deleteUser($dbh,$login);
            echo <<<FIN
            <div class="card" style="width: 50rem;">
                <div class='card-body'>
                Votre compte a bien été supprimé. Bonne continuation !
                <a href="index.php">Retour au site</a>
                </div>
            </div>
            FIN;
            $form_values_valid = true;
            logOut();
        }
        else {echo '<span class="avertissement">Mot de passe incorrect.</span>';}
    }
    
}

if (!$form_values_valid) {
    printDeleteUserForm();
}
?>

