<?php

$values_form_valid = false;

if (isset($_POST["depart"]) && $_POST["depart"] != "" &&
        isset($_POST["latitude_dep"]) && $_POST["latitude_dep"] != "" &&
        isset($_POST["longitude_dep"]) && $_POST["longitude_dep"] != "" &&
        isset($_POST["arrivee"]) && $_POST["arrivee"] != "" &&
        isset($_POST["latitude_arr"]) && $_POST["latitude_arr"] != "" &&
        isset($_POST["longitude_arr"]) && $_POST["longitude_arr"] != "" &&
        isset($_POST["debut"]) &&
        isset($_POST["fin"]) &&
        isset($_POST["date"]) &&
        isset($_POST["places"]) && $_POST["places"] > 0
) {
    $email = $_SESSION["email"]; #existe car l'utilisateur est connecté
    $depart = $_POST["depart"];
    $latitude_dep=$_POST["latitude_dep"] ;
    $longitude_dep=$_POST["longitude_dep"];
    $arrivee = $_POST["arrivee"];
    $latitude_arr=$_POST["latitude_arr"] ;
    $longitude_arr=$_POST["longitude_arr"];
    $debut = $_POST["debut"];
    $fin = $_POST["fin"];
    $date = $_POST["date"];
    $places = $_POST["places"];
    if (isset($_POST["lundi_pro"])) {
        $lundi_pro = $_POST["lundi_pro"];
    } else {
        $lundi_pro = 0;
    }
    if (isset($_POST["mardi_pro"])) {
        $mardi_pro = $_POST["mardi_pro"];
    } else {
        $mardi_pro = 0;
    }
    if (isset($_POST["mercredi_pro"])) {
        $mercredi_pro = $_POST["mercredi_pro"];
    } else {
        $mercredi_pro = 0;
    }
    if (isset($_POST["jeudi_pro"])) {
        $jeudi_pro = $_POST["jeudi_pro"];
    } else {
        $jeudi_pro = 0;
    }
    if (isset($_POST["vendredi_pro"])) {
        $vendredi_pro = $_POST["vendredi_pro"];
    } else {
        $vendredi_pro = 0;
    }
    if (isset($_POST["samedi_pro"])) {
        $samedi_pro = $_POST["samedi_pro"];
    } else {
        $samedi_pro = 0;
    }
    if (isset($_POST["dimanche_pro"])) {
        $dimanche_pro = $_POST["dimanche_pro"];
    } else {
        $dimanche_pro = 0;
    }
    if (isset($_POST["etapes"])) {
        $etapes = $_POST["etapes"];
    } else {
        $etapes = null;
    }
    if (isset($_POST["infosupp"])) {
        $infosupp = $_POST["infosupp"];
    } else {
        $infosupp = "";
    }
    
    if (isset($_POST["kilometrage"])) {
        $kilometrage = $_POST["kilometrage"];
    } else {
        $kilometrage = 0;
    }
    if (!(TrajetPropose::Collision($dbh, $email, $debut, $fin, $date))) {
        TrajetPropose::insererVoyagePro($dbh, $email, $depart,$latitude_dep,$longitude_dep, $arrivee,$latitude_arr,$longitude_arr, $debut, $fin, $date, $places, $lundi_pro,$mardi_pro,$mercredi_pro,$jeudi_pro,$vendredi_pro,$samedi_pro,$dimanche_pro, $etapes, $infosupp, $kilometrage);
        echo "<h2 class='centre'>Votre proposition de trajet a bien été enregistrée !</h2>";
        $values_form_valid = true;
    } else {
        echo "<h2 class='centre'>Vous avez déjà proposé ou accepté un voyage sur le même créneau !</h2>";
    }
    if ($lundi_pro != 0){
        
    }
} 


if (!$values_form_valid) {
    printProTrajetForm();
}

