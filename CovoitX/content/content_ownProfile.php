<?php
$user=Utilisateur::getUtilisateur($dbh,$_SESSION["email"]);

echo '<div class="card text-center">';
echo '<h5 class="card-header">Votre profil tel que vu par les autres utilisateurs :</h5>';
Utilisateur::afficher($user);

echo <<<FIN
    <div class="card text-center">
    <h5 class="card-header">Vous souhaitez modifier vos informations personnelles ?</h5>
    <div class="card-body">
    <a href="index.php?page=changePassword&todo=changePassword" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">Changer de mot de passe</a>
    <br>
    <br>
    <a href="index.php?page=deleteUser&todo=deleteUser" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">Supprimer mon compte</a>
    <br>
    <br>
    <a href="index.php?page=modifierinfos" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">Modifier mes infos</a>
    </div>
    </div>
    </div>
FIN;




?>

