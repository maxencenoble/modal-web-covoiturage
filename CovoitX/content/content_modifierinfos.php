<?php

$user=Utilisateur::getUtilisateur($dbh,$_SESSION["email"]);
$email=$_SESSION["email"];
$form_values_valid = false;
$photo_ok=false;

if (isset($_SESSION["email"]) && $_SESSION["email"] != "" && isset($_POST["password1"]) && $_POST["password1"] != ""){
    if ($user == NULL) {
        echo "Si vous n'êtes pas connectés, vous n'auriez jamais du finir ici !!";
    } else {
        if (utilisateur::testerMDP($dbh, $user, $_POST['password1'])) {
            if (isset($_POST["nom"])) {
                $nom = $_POST["nom"];
            }
            else {
               $nom =""; 
            }
            if (isset($_POST["prenom"])) {
                $prenom = $_POST["prenom"];
            }
            else {
               $prenom =""; 
            }
            if (isset($_POST["contact"])) {
                $contact = $_POST["contact"];
            }
            else {
               $contact =""; 
            }
            if (isset($_POST["naissance"])) {
                $naissance = $_POST["naissance"];
            }
            else {
               $naissance =""; 
            }
            
            if (isset($_POST["descriptif"])) {
                $descriptif = $_POST["descriptif"];
            }
            else {
               $descriptif =""; 
            }
            
            if (!empty($_FILES['photo']['tmp_name']) && is_uploaded_file($_FILES['photo']['tmp_name'])) {
            // Le fichier a bien été téléchargé
            list($larg,$haut,$type,$attr) = getimagesize($_FILES['photo']['tmp_name']);
            $allowedExtensions = array("jpg", "jpeg");
            $tmp=explode(".", $_FILES['photo']['name']);
            if (in_array(end($tmp), $allowedExtensions)){
                $taille_maxi = 100000;
                $taille = filesize($_FILES['photo']['tmp_name']);
                if($taille<$taille_maxi) {
                    $cle=sha1($email);
                    if (file_exists('images/photo_HD_'.$cle.'.jpg')){
                        // Si l'utilisateur a déjà une photo, il faut la supprimer d'abord
                        if (unlink('images/photo_HD_'.$cle.'.jpg')){
                            if (move_uploaded_file($_FILES['photo']['tmp_name'],'images/photo_HD_'.$cle.'.jpg')) {
                                $newWidth = 100;

                                // $photoHD est le chemin vers votre photo HD

                                list($widthOrig, $heightOrig) = getimagesize('images/photo_HD_'.$cle.'.jpg');

                                $ratio = $widthOrig / $newWidth;
                                $newHeight = $heightOrig / $ratio;

                                $tmpPhotoLD = imagecreatetruecolor($newWidth, $newHeight);
                                $image = imagecreatefromjpeg('images/photo_HD_'.$cle.'.jpg');
                                imagecopyresampled($tmpPhotoLD, $image, 0, 0, 0, 0, $newWidth, $newHeight, $widthOrig, $heightOrig);

                                // $photoLD est le chemin vers votre nouvelle photo LD

                                imagejpeg($tmpPhotoLD, 'images/photo_LD_'.$cle.'.jpg', 100);
                                $photo_ok=true;

                            } 
                            else {
                               echo "Erreur dans l'importation de votre photo : échec de la copie.";
                            }
                        }
                        else{
                            echo "Erreur dans l'importation de votre photo : échec dans la suppression de votre ancienne photo.";
                        }
                    }
                    else {
                       if (move_uploaded_file($_FILES['photo']['tmp_name'],'images/photo_HD_'.$cle.'.jpg')) {
                                $newWidth = 100;

                                // $photoHD est le chemin vers votre photo HD

                                list($widthOrig, $heightOrig) = getimagesize('images/photo_HD_'.$cle.'.jpg');

                                $ratio = $widthOrig / $newWidth;
                                $newHeight = $heightOrig / $ratio;

                                $tmpPhotoLD = imagecreatetruecolor($newWidth, $newHeight);
                                $image = imagecreatefromjpeg('images/photo_HD_'.$cle.'.jpg');
                                imagecopyresampled($tmpPhotoLD, $image, 0, 0, 0, 0, $newWidth, $newHeight, $widthOrig, $heightOrig);

                                // $photoLD est le chemin vers votre nouvelle photo LD

                                imagejpeg($tmpPhotoLD, 'images/photo_LD_'.$cle.'.jpg', 100);
                                $photo_ok=true;

                            } 
                            else {
                               echo "Erreur dans l'importation de votre photo : échec de la copie.";
                            } 
                    }
                }
                else{
                    echo "Erreur dans l'importation de votre photo : fichier trop volumineux!";
                }
            }
            else {
              echo "Erreur dans l'importation de votre photo : mauvais type de fichier. Nous n'accpetons que jpg ! Si besoin, réferez vous au site suivant : <a href:'https://image.online-convert.com/convert-to-jpg'>Convertisseur gratuit en .jpg</a>";
            }
        }
        if ($photo_ok){
            $photo=1;
        }
        else{$photo=$user->photo;}
            
            Utilisateur::modifier($dbh,$user,$nom,$prenom,$contact,$naissance,$descriptif,$photo);
            echo <<<FIN
            <div class="card" style="width: 50rem;">
                <div class='card-body'>
                Infos modifiées avec succès !
                <a href="index.php">Retour au site</a>
                </div>
            </div>
            FIN;
            $form_values_valid = true;
        }
        else {echo '<span class="avertissement">Mot de passe incorrect.</span>';}
    }
    
}

$user2=Utilisateur::getUtilisateur($dbh,$_SESSION["email"]);
echo '<div class="card text-center">';
echo '<h5 class="card-header">Votre profil actuel :</h5>';
echo '<div class="card-body">';
Utilisateur::afficher($user2);
echo '</div>';
echo '</div>';

if (!$form_values_valid) {
    echo "<br>";
    printModifierInfoForm($user);
}
