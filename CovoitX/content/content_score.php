<?php
if (isset($_SESSION["loggedIn"])) {
    $user = Utilisateur::getUtilisateur($dbh, $_SESSION["email"]);
    $note_demandeur= Utilisateur::getScoreEcoDemandeur($dbh,$user);
    $note_conducteur= Utilisateur::getScoreEcoConducteur($dbh,$user);
    $note_globale=0.5*($note_demandeur+$note_conducteur);
    if ($note_globale <10){
        $phrase="Vous pouvez faire mieux...";
    }
    if ($note_globale >=10 && $note_globale <40 ){
        $phrase="Vous êtes bien ...";
    }
    if ($note_globale >=40 && $note_globale <80 ){
        $phrase="Bravo pour votre investissement ...";
    }
    if ($note_globale >=100 ){
        $phrase="Excellent ...";
    }
    echo <<<FIN
    <div class="card text-center">
    <h5 class="card-header">Postez sur le site :</h5>
    <div class="card-body">
    
    <a href="index.php?page=trajetrecherche" class="btn btn-success active" role="button"  >Une demande de trajet</a>
    <br>
    <br>
    <a style="color:white" href="index.php?page=trajetpropose" class="btn btn-warning active" role="button" >Une offre de trajet</a>
    </div> 
    </div>
    
    
    <div class="card text-center">
    <div class="card-header">
    <h5> Votre score écologique </h5>
    <a style="font-weight:bold;" href="index.php?page=questions"><span class="centre">(en savoir plus)</span></a>
    </div>
    <div class="card-body">
    
    
    
    <div class="card text-center">
       <h5 class="card-header"><span class='centre-rouge'>Demandeur</span></h5>
        $note_demandeur /100
    </div>
    
    <div class="card text-center"> 
    <h5 class="card-header"><span class='centre-rouge'>Conducteur</span></h5>
     $note_conducteur /100
    
    </div>
    
    
    
    
    <div class="card text-center">
    <img class="card-img" src="https://image.freepik.com/vecteurs-libre/cartoon-nature-paysage-horizontal-transparente-beau-soir-matin-coucher-soleil-ciel-nuages-vector-illustration_1110-888.jpg" alt="Card image">
    <div class="card-img-overlay">
    <h3 class="card-text">$note_globale /100</h3>
    
    
    </div>
    </div>
    </div>
    </div>
    FIN;
    
}
else {
    //L'utilisateur n'est pas connecté
    echo <<<FIN
    <div class="card text-center" >
    <h5 class="card-header">Tentez l'aventure ! </h5>
    <div class="card-body">
    Faites-vous un réseau et développez vos voyages !
    </div>
    <img class="card-img-bottom" src="https://medias.liberation.fr/photo/703873-co20voiturage20copie.jpg?modified_at=1419881649&width=960" alt="Covoiturage">
    </div>
    FIN;
    
}


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

