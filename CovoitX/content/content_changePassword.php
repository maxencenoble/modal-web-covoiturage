<?php

$form_values_valid = false;

if (isset($_POST["email"]) && $_POST["email"] != "" && isset($_POST["password1"]) && $_POST["password1"] != "" && isset($_POST["oldpassword"]) && $_POST["oldpassword"] != "" && $_POST["password1"] == $_POST["password2"]) {
    $login = $_POST["email"];
    $user = utilisateur :: getUtilisateur($dbh, $login);
    if ($user == NULL) {
        echo '<span class="avertissement">Aucun utilisateur à ce nom.</span>';
    } else {
        if (utilisateur::testerMDP($dbh, $user, $_POST['oldpassword'])) {
            $newmdp = $_POST["password1"];
            utilisateur::modifierMdp($dbh,$user,$newmdp);
            echo <<<FIN
            <div class="card" style="width: 50rem;">
                <div class='card-body'>
                Mot de passe changer avec succès !
                <a href="index.php">Retour au site</a>
                </div>
            </div>
            FIN;
            $form_values_valid = true;
        }
        else {echo '<span class="avertissement">Mot de passe incorrect.</span>';}
    }
    
}
if (isset($_POST["password1"]) && isset($_POST["password2"])&&$_POST["password1"] != $_POST["password2"]){
    echo '<span class="avertissement">Les deux mots de passe diffèrent !</span>';
}

if (!$form_values_valid) {
    
    printChangePasswordForm();
}
?>

