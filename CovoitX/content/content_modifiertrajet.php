<?php

$form_values_valid = false;
if ((isset($_GET["type"])) && (isset($_GET["id"]))) {
    $type = ($_GET["type"]);
    $id = $_GET["id"];
    if (($type == 'pro') || ($type == 'rec')) {
        if ($type == 'pro') {
            $trajet = TrajetPropose::getTrajetPro($dbh, $id);
            $expectedemail = $trajet->conducteur;
        } else {
            $trajet = TrajetRecherche::getTrajetRec($dbh, $id);
            $expectedemail = $trajet->demandeur;
        }
        $email = $_SESSION["email"];
        if ($email == $expectedemail) {
            $form_values_valid = false;

            if (isset($_POST["depart"]) && $_POST["depart"] != "" &&
                    isset($_POST["latitude_dep"]) && $_POST["latitude_dep"] != "" &&
                    isset($_POST["longitude_dep"]) && $_POST["longitude_dep"] != "" &&
                    isset($_POST["arrivee"]) && $_POST["arrivee"] != "" &&
                    isset($_POST["latitude_arr"]) && $_POST["latitude_arr"] != "" &&
                    isset($_POST["longitude_arr"]) && $_POST["longitude_arr"] != "" &&
                    isset($_POST["debut"]) &&
                    isset($_POST["fin"]) &&
                    ($_POST["debut"] < $_POST["fin"]) &&
                    isset($_POST["date"])) {
                if ($type == 'pro') {
                    $depart = $_POST["depart"];
                    $latitude_dep = $_POST["latitude_dep"];
                    $longitude_dep = $_POST["longitude_dep"];
                    $arrivee = $_POST["arrivee"];
                    $latitude_arr = $_POST["latitude_arr"];
                    $longitude_arr = $_POST["longitude_arr"];
                    $debut = $_POST["debut"];
                    $fin = $_POST["fin"];
                    $date = $_POST["date"];
                    $places = $_POST["places"];
                    if (isset($_POST["lundi_pro"])) {
                        $lundi_pro = $_POST["lundi_pro"];
                    } else {
                        $lundi_pro = 0;
                    }
                    if (isset($_POST["mardi_pro"])) {
                        $mardi_pro = $_POST["mardi_pro"];
                    } else {
                        $mardi_pro = 0;
                    }
                    if (isset($_POST["mercredi_pro"])) {
                        $mercredi_pro = $_POST["mercredi_pro"];
                    } else {
                        $mercredi_pro = 0;
                    }
                    if (isset($_POST["jeudi_pro"])) {
                        $jeudi_pro = $_POST["jeudi_pro"];
                    } else {
                        $jeudi_pro = 0;
                    }
                    if (isset($_POST["vendredi_pro"])) {
                        $vendredi_pro = $_POST["vendredi_pro"];
                    } else {
                        $vendredi_pro = 0;
                    }
                    if (isset($_POST["samedi_pro"])) {
                        $samedi_pro = $_POST["samedi_pro"];
                    } else {
                        $samedi_pro = 0;
                    }
                    if (isset($_POST["dimanche_pro"])) {
                        $diamnche_pro = $_POST["dimanche_pro"];
                    } else {
                        $dimanche_pro = 0;
                    }
                    if (isset($_POST["etapes"])) {
                        $etapes = $_POST["etapes"];
                    } else {
                        $etapes = null;
                    }
                    if (isset($_POST["infosupp"])) {
                        $infosupp = $_POST["infosupp"];
                    } else {
                        $infosupp = "";
                    }

                    if (isset($_POST["kilometrage"])) {
                        $kilometrage = $_POST["kilometrage"];
                    } else {
                        $kilometrage = 0;
                    }
                    TrajetPropose::modifier($dbh, $depart, $latitude_dep, $longitude_dep, $arrivee, $latitude_arr, $longitude_arr, $debut, $fin, $date, $places, $lundi_pro, $mardi_pro, $mercredi_pro, $jeudi_pro, $vendredi_pro, $samedi_pro, $dimanche_pro, $etapes, $infosupp, $id);
                }
                if ($type == 'rec') {

                    $email = $_SESSION["email"]; #existe car l'utilisateur est connecté
                    $depart = $_POST["depart"];
                    $latitude_dep = $_POST["latitude_dep"];
                    $longitude_dep = $_POST["longitude_dep"];
                    $arrivee = $_POST["arrivee"];
                    $latitude_arr = $_POST["latitude_arr"];
                    $longitude_arr = $_POST["longitude_arr"];
                    $debut = $_POST["debut"];
                    $fin = $_POST["fin"];
                    $date = $_POST["date"];
                    $categorie = $_POST["categorie"];
                    if (isset($_POST["lundi_rec"])) {
                        $lundi_rec = $_POST["lundi_rec"];
                    } else {
                        $lundi_rec = 0;
                    }
                    if (isset($_POST["mardi_rec"])) {
                        $mardi_rec = $_POST["mardi_rec"];
                    } else {
                        $mardi_rec = 0;
                    }
                    if (isset($_POST["mercredi_rec"])) {
                        $mercredi_rec = $_POST["mercredi_rec"];
                    } else {
                        $mercredi_rec = 0;
                    }
                    if (isset($_POST["jeudi_rec"])) {
                        $jeudi_rec = $_POST["jeudi_rec"];
                    } else {
                        $jeudi_rec = 0;
                    }
                    if (isset($_POST["vendredi_rec"])) {
                        $vendredi_rec = $_POST["vendredi_rec"];
                    } else {
                        $vendredi_rec = 0;
                    }
                    if (isset($_POST["samedi_rec"])) {
                        $samedi_rec = $_POST["samedi_rec"];
                    } else {
                        $samedi_rec = 0;
                    }
                    if (isset($_POST["dimanche_rec"])) {
                        $dimanche_rec = $_POST["dimanche_rec"];
                    } else {
                        $dimanche_rec = 0;
                    }
                    if (isset($_POST["infosupp"])) {
                        $infosupp = $_POST["infosupp"];
                    } else {
                        $infosupp = "";
                    }
                    TrajetRecherche::modifier($dbh, $depart,$latitude_dep,$longitude_dep, $arrivee,$latitude_arr,$longitude_arr, $debut, $fin, $date, $categorie, $lundi_rec,$mardi_rec,$mercredi_rec,$jeudi_rec,$vendredi_rec,$samedi_rec,$dimanche_rec, $infosupp, $id);
                }
                echo <<<FIN
                <div class="card" style="width: 50rem;">
                    <div class='card-body'>
                    Infos modifiées avec succès !
                    <a href="index.php">Retour au site</a>
                    </div>
                </div>
                FIN;
                $form_values_valid = true;
            }

            if (!$form_values_valid) {
                if ($type=='pro'){
                    printModifierProTrajetForm($trajet);
                }
                if ($type=='rec'){
                    printModifierRecTrajetForm($trajet);
                }
            }
        } else {
            echo "Vous ne semblez pas être la personne adaptée pour modifier ce trajet ...";
        }
    } else {
        echo"Désolé, on ne peut pas afficher ça !";
    }
} else {
    echo"Désolé, rien à afficher !";
}


