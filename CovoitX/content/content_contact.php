<?php

echo <<<FIN
<div class="card text-center">
    <h5 class="card-header">Si vous rencontrez des difficultés dans l'utilisation du site, contactez-nous !</h5>
    <div class="card-body">

    <div class="card-deck text-center">

    <div class="card" style="max-width:280px;" >
    <img class="card-img-top" src="https://www.frankiz.net/image/big/43094"  alt="Photographie Antoine">
    <div class="card-body">
      <h5 class="card-title">Antoine BULTEL</h5>
      <p class="card-text">Elève de l'Ecole Polytechnique - 2ème année</p>
    </div>
    <div class="card-footer">
      <small class="text-muted">antoine.bultel@polytechnique.edu</small>
    </div>
    </div>
    
    <div class="card" style="max-width:280px;" >
    <img class="card-img-top" src="https://www.frankiz.net/image/big/41982"  alt="Photographie Maxence">
    <div class="card-body">
      <h5 class="card-title">Maxence NOBLE</h5>
      <p class="card-text">Elève de l'Ecole Polytechnique - 2ème année</p>
    </div>
    <div class="card-footer">
      <small class="text-muted">maxence.noble-bourillot@polytechnique.edu </small>
    </div>
    </div>
    

    </div>
    </div>
    
</div>
FIN;

