<?php

$user = Utilisateur::getUtilisateur($dbh, $_SESSION["email"]);

if (isset($_GET['todo']) && $_GET['todo'] == "delete" &&
        isset($_GET['id'])) {

    $id = $_GET['id'];
    $trajet = TrajetPropose::getTrajetPro($dbh, $_GET['id']);
    //On vérifie que l'utilisateur a bien le droit de supprimer le trajet ...
    $expectedemail=$trajet->conducteur;
    if ($user->email==$expectedemail){
        TrajetPropose::delete($dbh,$trajet->voyagepro);
    }
    else {
        echo "Impossile de supprimer ce trajet";
    }
}

$aAfficherPro = TrajetPropose::getTrajetsProUser($dbh, $user->email);

if (isset($_POST["id_voyage"]) && $_POST["id_voyage"] != "" &&
        isset($_POST["statut_voyage"]) && $_POST["statut_voyage"] != "" &&
        isset($_POST["client"]) && $_POST["client"] != "") {
    $email = $_SESSION["email"]; #existe car l'utilisateur est connecté
    $id_voyage = $_POST["id_voyage"];
    $statut_voyage = $_POST["statut_voyage"];
    $client = $_POST["client"];
    if ($client == "propose") {
        if ($statut_voyage == "non_accepte") {
            TrajetPropose::accepterTrajet($dbh, $email, $id_voyage, $client);
            $_POST = array();
            } else {
            TrajetPropose::refuserTrajet($dbh, $email, $id_voyage, $client);
            $_POST = array();
             }
    } else {
        if ($statut_voyage == "non_accepte") {
            TrajetRecherche::accepterTrajet($dbh, $email, $id_voyage, $client);
            $_POST = array();
            } else {
            TrajetRecherche::refuserTrajet($dbh, $email, $id_voyage, $client);
            $_POST = array();
            }
    }
}



echo "<div class='card text-center'>";
echo "<div class='card-header'>";
echo <<<FIN
<ul class="nav nav-tabs card-header-tabs justify-content-center">
      <li class="nav-item">
        <a class="nav-link" href="index.php?page=mestrajets">Comment ça fonctionne ?</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" href="index.php?page=mestrajetspro">Mettre à jour mes propositions</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?page=mestrajetsrec">Mettre à jour mes recherches</a>
      </li>
</ul>
FIN;
echo "</div>";
echo "<div class='card-body'>";


foreach ($aAfficherPro as $trajet) {
    echo "<div class='card text-center'>";
    $login = $user->email;
    $id = $trajet->voyagepro;
    $accepte = TrajetPropose::estAccepte($dbh, $login, $id);
    if ($accepte) {
        $client = "propose";
        $statut_voyage = "accepte";
        echo <<<FIN
        <h4 class='card-header' style='background-color:green'> Accepté </h4>
        <form action="index.php?page=mestrajetspro&todo=statut" method="post">
        <div class='statut'>
            <input type=hidden class="form-control" name="id_voyage" value=$id />
            <input type=hidden class="form-control" name="statut_voyage" value=$statut_voyage />
            <input type=hidden class="form-control" name="client" value=$client />
        </div>
        <br>
        <button type='submit' class ='btn btn-warning'> De nouveau libre ?</button>
        </form>
        FIN;
    } else {
        $client = "propose";
        $statut_voyage = "non_accepte";
        echo <<<FIN
        <h4 class='card-header' style='background-color:red'> Pas encore accepté </h4>
        <form action="index.php?page=mestrajetspro&todo=statut" method="post">
        <div class='statut'>
            <input type=hidden class="form-control" name="id_voyage" value=$id />
            <input type=hidden class="form-control" name="statut_voyage" value=$statut_voyage />
            <input type=hidden class="form-control" name="client" value=$client />
        </div>
        <br>
        <button type='submit' class ='btn btn-warning'> Accepté ?</button>
        </form>
        <form action="index.php?page=matching&todo=matching" method="post">
        <div class='matching'>
        <input type=hidden class="form-control" name="id_voyage" value=$id />
        <input type=hidden class="form-control" name="client" value=$client />
        </div>
        <br>
        <button type='submit' class ='btn btn-warning'> Trouver les meilleures offres </button>
        </form>
        FIN;
    }
    echo <<<FIN
    
    <div class='card-body'>
    FIN;
    TrajetPropose::afficherPersonal($dbh, $trajet->voyagepro);
    if (!$accepte){
    echo <<<FIN
    <br>
    <a href="index.php?page=modifiertrajet&type=pro&id=$id" class="btn btn-warning"> Modifier</a>
    <a href="index.php?page=mestrajetspro&todo=delete&id=$id" class="btn btn-warning"> Supprimer</a>
    <br>
    </div>
    </div>
    <br>
    FIN;}
    else{
    echo <<<FIN
    </div>
    </div>
    <br>
    FIN;
    }
}


echo "</div>";
echo "</div>";

