<?php
if (isset($_GET['todo']) && $_GET['todo'] == "delete" &&
        isset($_GET['type']) && (($_GET['type'] == 'pro') || ($_GET['type'] == 'rec')||($_GET['type']=='user')) &&
        isset($_GET['id'])) {
    $type = $_GET['type'];
    $id = $_GET['id'];
    
    if (isset($_POST["raison"])) {
        $raison=$_POST["raison"];
    }
    else {
        $raison="";
    }
    if ($type == 'pro') {
        //Envoi de mail : ssi vous avez rempli la bonne ligen dans index.php
        //$trajet= TrajetPropose::getTrajetPro($dbh,$id);
        //mail($trajet->conducteur,"Suppression d'une de vos propositions de trajet",
                //"Bonjour, nous sommes au regret de vous informer qu'une de vos proporistion de trajet a été supprimée "
                //. "de notre site par un administrateur, pour la raison suivante : ".$raison
                //.". Bonne journée, L'équipe du site Covoiturage Polytechnique.");
        TrajetPropose::delete($dbh, $id);
    }
    if ($type == 'rec') {
        //Envoi de mail : ssi vous avez rempli la bonne ligen dans index.php
        //$trajet= TrajetRecherche::getTrajetRec($dbh,$id);
        //mail($trajet->demandeur,"Suppression d'une de vos demandes de trajet",
                //"Bonjour, nous sommes au regret de vous informer qu'une de vos demandes de trajet a été supprimée "
                //. "de notre site par un administrateur, pour la raison suivante : ".$raison
                //.". Bonne journée, L'équipe du site Covoiturage Polytechnique.");
        TrajetRecherche::delete($dbh, $id);
    }
    if ($type == 'user') {
        //Envoi de mail : ssi vous avez rempli la bonne ligen dans index.php
        //$trajet= TrajetRecherche::getTrajetRec($dbh,$id);
        //mail($id,"Suppression d'une de vos demandes de trajet",
                //"Bonjour, nous sommes au regret de vous informer qu'une de vos demandes de trajet a été supprimée "
                //. "de notre site par un administrateur, pour la raison suivante : ".$raison
                //.". Bonne journée, L'équipe du site Covoiturage Polytechnique.");
        Utilisateur::deleteUser($dbh,$id);
    }
}

echo <<<FIN
<div class="card text-center">
    <div class="card-header"><h5> Bienvenue dans la zone administrateur.</h5></div>
    <div class="card-body">
    <p> Vous avez ici le pouvoir d'accéder à et de supprimer n'importe quel trajet ou utilisateur.<br>
        Votre mission est d'éviter que tout contenu choquant ou dégradant soit publié sur le site par des petits malins ...<br>
        Un grand pouvoir implique de grandes responsabilités : bon courage !</p>
    </div>
</div>
FIN;

//1er tableau---------------------------------------------------------------------
echo"<div class='card text-center'>";
echo "<h5 class='card-header'> Trajets proposés par les utilisateurs du site</h5>";
echo "<div class='card-body'>";
$data = TrajetPropose::getAllVoyagePro($dbh);
?>

<table id="admintrajets1" class="display">
    <thead>
        <tr>
            <th>Date</th>
            <th>Proposé par</th>
            <th>Infos</th>
            <th></th>
        </tr>
    </thead>
    <tbody id="body1">
        <?php
        foreach ($data as $trajet) {
            $email = $trajet->conducteur;
            $user = Utilisateur::getUtilisateur($dbh, $email);
            $prenom = $user->prenom;
            $info_supp_pro = $trajet->infosupp;
            $voyagepro = $trajet->voyagepro;
            $datejours = TrajetPropose::tous_les_pro($trajet);
            if (!TrajetPropose::estAccepte($dbh, $email, $voyagepro)) {
                echo <<<FIN
    <tr>
        <td>$datejours</td>
        <td>$user->nom $user->prenom ($email)</td>
        <td>$info_supp_pro</td>
        <td><button type="button"  id="$voyagepro" class ="btn-admin_pro btn btn-danger"> En savoir plus</button></td>
    </tr>
    FIN;
            }
        }
        ?>
    </tbody>
</table>

<?php
foreach ($data as $trajet) {
    $str = '<div class="zoomadmin" id="zoomadminpro' . $trajet->voyagepro . '">';
    echo $str;
    echo
    TrajetPropose::afficher($dbh, $trajet->voyagepro);
    printAdminDeleteForm($dbh, 'pro', $trajet);
    echo"</div>";
}
echo "</div></div>";
//--------------------------------------------------------------------------------------
//2e tableau ---------------------------------------------------------------------------
echo"<div class='card text-center'>";
echo "<h5 class='card-header'> Trajets proposés par les utilisateurs du site</h5>";
echo "<div class='card-body'>";
$data2 = TrajetRecherche::getAllVoyageRec($dbh);
?>

<table id="admintrajets2" class="display">
    <thead>
        <tr>
            <th>Date</th>
            <th>Proposé par</th>
            <th>Infos</th>
            <th></th>
        </tr>
    </thead>
    <tbody id="body2">
        <?php
        foreach ($data2 as $trajet) {
            $email = $trajet->demandeur;
            $user = Utilisateur::getUtilisateur($dbh, $email);
            $prenom = $user->prenom;
            $info_supp_rec = $trajet->infosupp;
            $voyagerec = $trajet->voyagerec;
            $datejours = TrajetRecherche::tous_les_rec($trajet);
            if (!TrajetRecherche::estAccepte($dbh, $email, $voyagerec)) {
                echo <<<FIN
    <tr>
        <td>$datejours</td>
        <td>$user->nom $user->prenom ($email)</td>
        <td>$info_supp_rec</td>
        <td><button type="button"  id="$voyagerec" class ="btn-admin_rec btn btn-danger"> En savoir plus</button></td>
    </tr>
    FIN;
            }
        }
        ?>
    </tbody>
</table>

<?php
foreach ($data2 as $trajet) {
    $str = '<div class="zoomadmin" id="zoomadminrec' . $trajet->voyagerec . '">';
    echo $str;
    echo
    TrajetRecherche::afficher($dbh, $trajet->voyagerec);
    printAdminDeleteForm($dbh, 'rec', $trajet);
    echo"</div>";
}
echo "</div></div>";

//--------------------------------------------------------------------------------------
//3e tableau ---------------------------------------------------------------------------
echo"<div class='card text-center'>";
echo "<h5 class='card-header'> Utilisateurs du site</h5>";
echo "<div class='card-body'>";
$data3 = Utilisateur::getAll($dbh);
?>

<table id="adminusers" class="display">
    <thead>
        <tr>
            <th>Email</th>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Contact</th>
            <th>Description</th>
            <th></th>
        </tr>
    </thead>
    <tbody id="body3">
<?php
foreach ($data3 as $user) {
    $email = $user->email;
    $prenom = $user->prenom;
    $nom=$user->nom;
    $contact=$user->contact;
    $descriptif=$user->descriptif;
    
    echo <<<FIN
    <tr>
        <td>$email</td>
        <td>$nom</td>
        <td>$prenom</td>
        <td>$contact</td>
        <td>$descriptif</td>
        <td><button type="button"  id="$email" class ="btn-admin_user btn btn-danger"> En savoir plus</button></td>
    </tr>
    FIN;
    
}
?>
    </tbody>
</table>

<?php
foreach ($data3 as $user) {
    $str = '<div class="zoomadmin" id="zoomuser'.sha1($user->email).'">';
    echo $str;
    Utilisateur::afficher($user);
    printAdminDeleteForm($dbh, 'user', $user);
    echo "</div>";
}
 echo"</div></div>";

?>



