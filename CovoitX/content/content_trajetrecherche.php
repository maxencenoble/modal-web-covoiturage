<?php
$values_form_valid=false;

if (isset($_POST["depart"]) && $_POST["depart"] != "" &&
        isset($_POST["latitude_dep"]) && $_POST["latitude_dep"] != "" &&
        isset($_POST["longitude_dep"]) && $_POST["longitude_dep"] != "" &&
        isset($_POST["arrivee"]) && $_POST["arrivee"] != "" &&
        isset($_POST["latitude_arr"]) && $_POST["latitude_arr"] != "" &&
        isset($_POST["longitude_arr"]) && $_POST["longitude_arr"] != "" &&
        isset($_POST["debut"]) &&
        isset($_POST["fin"]) &&
        isset($_POST["date"]) &&
        isset($_POST["categorie"])
) {
    $email=$_SESSION["email"]; #existe car l'utilisateur est connecté
    $depart = $_POST["depart"];
    $latitude_dep=$_POST["latitude_dep"] ;
    $longitude_dep=$_POST["longitude_dep"];
    $arrivee = $_POST["arrivee"];
    $latitude_arr=$_POST["latitude_arr"] ;
    $longitude_arr=$_POST["longitude_arr"];
    $debut = $_POST["debut"];
    $fin = $_POST["fin"];
    $date = $_POST["date"];
    $categorie=$_POST["categorie"];
    if (isset($_POST["lundi_rec"])) {
        $lundi_rec = $_POST["lundi_rec"];
    } else {
        $lundi_rec = 0;
    }
    if (isset($_POST["mardi_rec"])) {
        $mardi_rec = $_POST["mardi_rec"];
    } else {
        $mardi_rec = 0;
    }
    if (isset($_POST["mercredi_rec"])) {
        $mercredi_rec = $_POST["mercredi_rec"];
    } else {
        $mercredi_rec = 0;
    }
    if (isset($_POST["jeudi_rec"])) {
        $jeudi_rec = $_POST["jeudi_rec"];
    } else {
        $jeudi_rec = 0;
    }
    if (isset($_POST["vendredi_rec"])) {
        $vendredi_rec = $_POST["vendredi_rec"];
    } else {
        $vendredi_rec = 0;
    }
    if (isset($_POST["samedi_rec"])) {
        $samedi_rec= $_POST["samedi_rec"];
    } else {
        $samedi_rec = 0;
    }
    if (isset($_POST["dimanche_rec"])) {
        $dimanche_rec = $_POST["dimanche_rec"];
    } else {
        $dimanche_rec = 0;
    }
    if (isset($_POST["infosupp"])){
        $infosupp=$_POST["infosupp"];
    }else{
        $infosupp="";
    }
    if (!(TrajetRecherche::Collision($dbh,$email,$debut,$fin,$date))){
        TrajetRecherche::insererVoyageRec($dbh, $email, $depart,$latitude_dep,$longitude_dep,$arrivee,$latitude_arr,$longitude_arr,$debut,$fin,$date,$categorie,$lundi_rec,$mardi_rec,$mercredi_rec,$jeudi_rec,$vendredi_rec,$samedi_rec,$dimanche_rec,$infosupp);
        echo "<h2 class='centre'>Votre recherche de trajet a bien été enregistrée !</h2>";
        $values_form_valid=true;
    } else{
        echo "<h2 class='centre'>Vous avez déjà proposé ou accepté un voyage sur le même créneau !</h2>";
    }
}

if(!$values_form_valid){
    printRecTrajetForm();
}

