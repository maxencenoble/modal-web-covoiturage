<?php
//Deux pages différentes en fonction de si l'utilisateur est connecté ou non

if (!isset($_SESSION["loggedIn"])) {
    echo <<<FIN
    <div class="card text-center">
    <h5 class="card-header">Voici quelques questions et leurs réponses susceptibles de vous intéresser... </h5>
    <div class="card-body">
    <div class="card-columns">
    <div class="card text-center">
    <h6 class="card-header"> Puis-je utiliser le site sans être inscrit ? </h6>
    <br>
    <h4 class="card-title"> NON ! </h4>
    <div class="card-body">
    Ce site est conçu pour mettre en relation des voyageurs avec des impératifs horaires dans le cadre d'un environnement de travail. Votre inscription réalisée sous le contrôle d'un modérateur constitue en quelque sorte une assurance contre une utilisation malveillante.
    </div>
    </div>
    
    <div class="card text-center">
    <h6 class="card-header"> L'inscription est-elle gratuite ? </h6>
    <br>
    <h4 class="card-title"> OUI ! </h4>
    <div class="card-body">
    Ce service est 100% gratuit, et ne requiert aucune donnée bancaire.
    </div>
    </div>
    
    <div class="card text-center">
    <h6 class="card-header"> Dois-je avoir mon permis pour m'inscrire ? </h6>
    <br>
    <h4 class="card-title"> NON ! </h4>
    <div class="card-body">
    Vous pouvez vous inscrire pour simplement demander des trajets sans avoir à conduire en retour.
    </div>
    </div>
    
    <div class="card text-center">
    <h6 class="card-header"> Quelle est la plus-value de ce site par rapport à BlaBlaCar ? </h6>
    <br>
    <h4 class="card-title"> La précision ! </h4>
    <div class="card-body">
    Grâce à ce site, vous pouvez avoir un contrôle extrêmement précis sur la zone de départ ou d'arrivée sur le plateau de Saclay, ce qui n'est pas toujours le cas pour des cas classiques de covoiturage.
    <br>
    <br>
    De plus, vous pouvez très facilement repérer les trajets effectués régulièrement sur les routes que vous souhaitez emprunter et vous créer par cette occasion un carnet de contacts de voyageurs qui vous ressemblent.
    </div>
    </div>
    
    
    </div>
    </div>
    </div>
    FIN;} 
else {
    echo <<<FIN
    <div class="card text-center">
    <h5 class="card-header">Voici quelques questions et leurs réponses susceptibles de vous intéresser... </h5>
    <div class="card-body">
    <div class="card-columns">
    
    <div class="card text-center">
    <h6 class="card-header"> Si je suis pressé, comment puis-je trouver rapidement un trajet qui me convienne ? </h6>
    <div class="card-body">
    <div class="card text-center">
    <h6 class="card-header">1- Allez dans l'onglet "Voir tous les trajets" </h6>
    <div class="card-body"> De la sorte vous avez accès à toutes les propositions de trajet et toutes les recherches de trajet enregistrées sur ce site. En ordonnant la colonne qui vous intéresse ou en recherchant un caractère spécial, vous pouvez ainsi trouver le trajet qui vous intéresse.</div>
    </div>
    <div class="card text-center">
    <h6 class="card-header">2- Cliquez sur "En savoir plus" </h6>
    <div class="card-body"> De cette façon, vous pouvez accéder avec précision au lieu de départ proposé/recherché et aux informations personnelles de l'utilisateur à l'origine de cette proposition/recherche.</div>
    </div>
    </div>
    </div>
    
    <div class="card text-center p-3">
    <h6 class="card-header"> Puis-je modifier les informations des trajets que j'ai enregistrés ? </h6>
    <div class="card-body">
    Dans l'onglet "Voir mes Trajets", vous pouvez sélectionner votre trajet et modifier ses caractéristiques (pourvu qu'il ne soit pas déjà accepté).
    <br>    
    Vous pouvez également supprimer ce trajet.
    </div>
    </div>
    
    
    <div class="card text-center">
    <h6 class="card-header"> Si je veux planifier des trajets réguliers pour une recherche ou une proposition de trajet, comment faire ? </h6>
    <div class="card-body">
    <div class="card text-center">
    <h6 class="card-header">1- Cliquez sur "Rechercher/Proposer un trajet?" </h6>
    <div class="card-body"> Vous aurez à remplir un formulaire assez précis qui vous assurera de trouver avec précision un trajet qui corresponde à vos envies.</div>
    </div>
    <div class="card text-center">
    <h6 class="card-header">2- Allez dans l'onglet "Voir mes trajets" </h6>
    <div class="card-body"> Sélectionner le trajet que vous venez de poster sur le site (2 onglets : recherché ou proposé) et cliquez sur "Trouver les meilleures offres". Une liste de trajets vous sera alors proposée suivant des critères d'exigence précis que vous pourrez étendre dans un deuxième temps.</div>
    </div>
    <div class="card text-center">
    <h6 class="card-header">3- Cliquez sur "En savoir plus" </h6>
    <div class="card-body"> De cette façon, vous pouvez accéder avec précision au lieu de départ proposé/recherché et aux informations personnelles de l'utilisateur à l'origine de cette proposition/recherche.</div>
    </div>
    </div>
    </div>
    
    <div class="card text-center p-3">
    <h6 class="card-header"> Puis-je modifier mes informations personnelles ? </h6>
    <div class="card-body">
    En vous rendant dans la zone de votre profil, vous avez la possibilité de modifier les informations que vous avez entrées pour vous inscrire. 
    <br>    
    Vous pouvez également supprimer votre compte.
    </div>
    </div>
    
    <div class="card text-center">
    <h6 class="card-header"> Une fois le trajet trouvé, que dois-je faire ? </h6>
    <div class="card-body">
    <div class="card text-center">
    <h6 class="card-header">1- Rentrez en contact avec le conducteur/demandeur </h6>
    <div class="card-body"> Les informations présentes sur le profil de l'utilisateur en question vous permettront de le contacter ; le reste se joue alors entre vous seuls, ce site n'étant qu'un intermédiaire.</div>
    </div>
    <div class="card text-center">
    <h6 class="card-header">2- Actualisez le statut de votre trajet </h6>
    <div class="card-body"> En cliquant sur "Accepté ?", vous retirerez votre trajet de la liste des trajets proposés/recherchés sur le site et mettrez à jour votre score écologique.</div>
    </div>
    </div>
    </div>
    
    <div class="card text-center">
    <h6 class="card-header"> A quoi correspond exactement le score écologique ? </h6>
    <div class="card-body">
    Ce score a été élaboré pour inciter les utilisateurs du site à proposer/rechercher des trajets activement : 
    <br>
    non seulement il récompense les utilisateurs aux nombreuses demandes et nombreuses offres, mais prend également en compte la quantité de mises en relation effectives réalisées sur ce site. 
    <br>
    Votre objectif est donc de le maximiser !
    </div>
    </div>
    
    
    
    
    
    </div>
    </div>
    </div>
    FIN;
        }

