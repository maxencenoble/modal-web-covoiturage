<?php

require('utilities/printForms.php');

//printRegisterForm();




$form_values_valid = false;
$photo_ok = false;


// passer en obligatoire les champs nom prénom etc.
if (isset($_POST["email"]) && $_POST["email"] != "" &&
        isset($_POST["password1"]) && isset($_POST["password2"]) &&
        $_POST["password1"] != "" && $_POST["password1"] == $_POST["password2"]) {
    $email = $_POST["email"];
    $user = utilisateur :: getUtilisateur($dbh, $email);
    if ($user != NULL) {
        echo "Un compte existe déjà avec cet email.";

        //ajouter lien vers mot de passe oublié
    } else {
        if (isset($_POST["nom"])) {
            $nom = $_POST["nom"];
        }
        if (isset($_POST["prenom"])) {
            $prenom = $_POST["prenom"];
        }
        if (isset($_POST["contact"])) {
            $contact = $_POST["contact"];
        }
        if (isset($_POST["naissance"])) {
            $naissance = $_POST["naissance"];
        }


        if (isset($_POST["descriptif"])) {
            $descriptif = $_POST["descriptif"];
        }
        
        //Gestion de la photo de profil
        if (!empty($_FILES['photo']['tmp_name']) && is_uploaded_file($_FILES['photo']['tmp_name'])) {
            list($larg, $haut, $type, $attr) = getimagesize($_FILES['photo']['tmp_name']);
            $allowedExtensions = array("jpg", "jpeg");
            $tmp = explode(".", $_FILES['photo']['name']);
            if (in_array(end($tmp), $allowedExtensions)) {
                $taille_maxi = 500000;
                $taille = filesize($_FILES['photo']['tmp_name']);
                if ($taille < $taille_maxi) {
                    $cle = sha1($email);
                    if (move_uploaded_file($_FILES['photo']['tmp_name'], 'images/photo_HD_' . $cle . '.jpg')) {
                        //Création du fichier Low Definition
                        $newWidth = 100;
                        list($widthOrig, $heightOrig) = getimagesize('images/photo_HD_' . $cle . '.jpg');
                        $ratio = $widthOrig / $newWidth;
                        $newHeight = $heightOrig / $ratio;
                        $tmpPhotoLD = imagecreatetruecolor($newWidth, $newHeight);
                        $image = imagecreatefromjpeg('images/photo_HD_' . $cle . '.jpg');
                        imagecopyresampled($tmpPhotoLD, $image, 0, 0, 0, 0, $newWidth, $newHeight, $widthOrig, $heightOrig);
                        imagejpeg($tmpPhotoLD, 'images/photo_LD_' . $cle . '.jpg', 100);
                        $photo_ok = true;
                    } else {
                        echo "Erreur dans l'importation de votre photo : echec de la copie";
                    }
                } else {
                    echo "Erreur dans l'importation de votre photo : fichier trop volumineux!";
                }
            } else {
                echo "Erreur dans l'importation de votre photo : mauvais type de fichier. Nous n'accpetons que jpg ! Si besoin, réferez vous au site suivant : <a href:'https://image.online-convert.com/convert-to-jpg'>Convertisseur gratuit en .jpg</a>";
            }
        }

        if ($photo_ok) {
            $photo = 1;
        } else {
            $photo = NULL;
        }
        $email = $_POST["email"];
        $mdp = $_POST["password1"];
        utilisateur::insererUtilisateur($dbh, $email, $nom, $prenom, $mdp, $naissance, $contact, $descriptif, $photo);
        echo <<<FIN
            <div class="card" style="width: 50rem;">
                <div class='card-body'>
                Votre compte a bien été créé !
                <a href="index.php">Retour au site</a>
                </div>
            </div>
            FIN;
        $form_values_valid = true;
    }
} else {
    //Différents messages d'erreurs pour tous les cas de figure.
    if (isset($_POST["password1"]) && isset($_POST["password2"]) && $_POST["password1"] != $_POST["password2"]) {
        echo "<p class='avertissement'> Les deux mots de passe diffèrent !</p>";
    }
}


if (!$form_values_valid) {
    if (!isset($_SESSION["loggedIn"])) {
        printRegisterForm();
    } else {

        echo <<<FIN
            <div class="card" style="width: 50rem;">
                <div class='card-body'>
                Votre compte a bien été créé !
                <a href="index.php">Retour au site</a>
                </div>
            </div>
            FIN;
    }
}




    

