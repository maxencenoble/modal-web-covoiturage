<?php

//Fonctions header et footer

function generateHTMLHeader($titre, $feuilleCSS) {

    echo<<<FIN
            <!DOCTYPE html>
            <head>
            <meta charset="UTF-8"/>
            <link rel="stylesheet" type="text/css" href="'.$feuilleCSS.'" />
            <meta name="author" content="Antoine BULTEL-Maxence NOBLE"/>
            <meta name="keywords" content="Covoiturage plateau de Saclay"/>
            <meta name="description" content="Covoiturage plateau de Saclay"/>
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <!--CSS Perso-->
            <link href="$feuilleCSS" rel="stylesheet">
            <!-- Bootstrap CSS -->
            <link href="css/bootstrap.min.css" rel="stylesheet">
            <!-- Optional JavaScript -->

            <!-- jQuery first, then Popper.js, then Bootstrap JS -->
            <script src="js/jquery.min.js"></script>
            <script src="js/popper.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <!--DataBase-->
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
            <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
            
FIN;
    echo '<title>' . $titre . '</title>';
    echo '</head>';
    echo '<body>';
    echo <<<FIN
    <div class="jumbotron-perso jumbotron jumbotron-fluid" style="margin-bottom: 0rem; padding: 2rem 2rem;">
        <div class = "row">
            <div class="offset-md-1 titre-site">
                <h1>COVOIT @ SACLAY</h1>
                <h3> Le site qui centralise et fluidifie l'offre et la demande de covoiturage sur le plateau</h3>
            </div>
        </div>
    </div>
    FIN;
}

function generateHTMLFooter() {

    echo '<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrGcDW157r1rsvtYgiJRJqDlJgk9b50-4&amp;callback=initMap"></script>';
    echo '<script src="js/moncode.js"></script>';
    echo '</body>';
    echo '</html>';
}

//Liste des pages et gestion de celles-ci

$page_list = [array(
"name" => "accueil",
 "title" => "Accueil de notre site",
 "menutitle" => "Accueil",
 "connecté" => FALSE,
 "admin" => FALSE),
    array(
        "name" => "contact",
        "title" => "Contact",
        "menutitle" => "Nous contacter",
        "connecté" => FALSE,
        "admin" => FALSE),
    array(
        "name" => "info",
        "title" => "Information pratiques",
        "menutitle" => "ERREUR",
        "connecté" => FALSE,
        "admin" => FALSE),
    array(
        "name" => "register",
        "title" => "Créer un compte",
        "menutitle" => "ERREUR",
        "connecté" => FALSE,
        "admin" => FALSE),
    array(
        "name" => "changePassword",
        "title" => "Changer de mot de passe",
        "menutitle" => "ERREUR",
        "connecté" => TRUE,
        "admin" => FALSE),
    array(
        "name" => "deleteUser",
        "title" => "Supprimer votre compte",
        "menutitle" => "ERREUR",
        "connecté" => TRUE,
        "admin" => FALSE),
    array(
        "name" => "questions",
        "title" => "Foire aux questions",
        "menutitle" => "Foire aux questions",
        "connecté" => FALSE,
        "admin" => FALSE),
    array(
        "name" => "trajetrecherche",
        "title" => "Faire une recherche de trajet",
        "menutitle" => "ERREUR",
        "connecté" => TRUE,
        "admin" => FALSE
    ),
    array(
        "name" => "trajetpropose",
        "title" => "Faire une proposition de trajet",
        "menutitle" => "ERREUR",
        "connecté" => TRUE,
        "admin" => FALSE
    ),
    array(
        "name" => "ownprofile",
        "title" => "Mon profil",
        "menutitle" => "ERREUR",
        "connecté" => TRUE,
        "admin" => FALSE
    ),
    array(
        "name" => "modifierinfos",
        "title" => "Modifier mes informations personelles",
        "menutitle" => "ERREUR",
        "connecté" => TRUE,
        "admin" => FALSE
    ),
    array(
        "name" => "mestrajets",
        "title" => "Toutes vos demandes et propositions",
        "menutitle" => "Voir mes trajets",
        "connecté" => TRUE,
        "admin" => FALSE
    ),
    array(
        "name" => "zoomtrajet",
        "title" => "Infos sur ce trajet",
        "menutitle" => "ERREUR",
        "connecté" => TRUE,
        "admin" => FALSE
    ),
    array(
        "name" => "voirtrajets",
        "title" => "Voir tous les trajets",
        "menutitle" => "Voir tous les trajets",
        "connecté" => TRUE,
        "admin" => FALSE
    ),
    array(
        "name" => "matching",
        "title" => "Recherche du meileur matching",
        "menutitle" => "ERREUR",
        "connecté" => TRUE,
        "admin" => FALSE
    ),
    array(
        "name" => "mestrajetspro",
        "title" => "Toutes vos propositions",
        "menutitle" => "ERREUR",
        "connecté" => TRUE,
        "admin" => FALSE
    ),
    array(
        "name" => "mestrajetsrec",
        "title" => "Toutes vos demandes",
        "menutitle" => "ERREUR",
        "connecté" => TRUE,
        "admin" => FALSE
    ),
    array(
        "name" => "modifiertrajet",
        "title" => "Modifier un trajet",
        "menutitle" => "ERREUR",
        "connecté" => TRUE,
        "admin" => FALSE
    ),
    array(
        "name" => "zoneadmin",
        "title" => "Zone administrateur",
        "menutitle" => "Zone administrateur",
        "connecté" => TRUE,
        "admin" => TRUE
    ),
    array(
        "name" => "score_details",
        "title" => "Détails sur le score",
        "menutitle" => "ERREUR",
        "connecté" => TRUE,
        "admin" => FALSE
    )
];

function generateMenu($page_list) {
    echo <<<FIN
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
    FIN;

    foreach ($page_list as $page) {
        if (($page["menutitle"] != "ERREUR") && 
                !($page["connecté"] && !(isset($_SESSION["loggedIn"]))) &&
                !($page["admin"] && !(isset($_SESSION["admin"])&&($_SESSION["admin"])))) {
            echo '<li class="nav-item"><a class="nav-link" href="index.php?page=' . $page["name"] . '">' . $page["menutitle"] . '<span class="sr-only">(current)</span></a></li>';
        }
    }
    echo <<<FIN
            </ul>
        </div>
    </nav>
    FIN;
}

function printPages($page_list) {
    foreach ($page_list as $page) {
        echo $page["name"];
    }
}

function checkPage($askedPage, $page_list) {
    foreach ($page_list as $page) {
        if ($page["name"] == $askedPage) {
            if ($page["connecté"] == TRUE) {
                if ($page["admin"] == TRUE){
                    return (isset($_SESSION["admin"]));
                }
                else  {
                    return (isset($_SESSION["loggedIn"]));
                }
            } else {
                return(TRUE);
            }
        }
    }

    return(FALSE);
}

function getPageTitle($askedPage, $page_list) {
    foreach ($page_list as $page) {
        if ($page["name"] == $askedPage) {
            return($page["title"]);
        }
    }
    return["Erreur"];
}
?>




