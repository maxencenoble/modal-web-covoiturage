<?php

if (!function_exists('printLoginForm')) {

    function printLoginForm($askedPage) {
        if (isset($_SESSION["email"])){
            $email=$_SESSION["email"];
        }else{
            $email="";
        }
        $askedPagesafe=htmlspecialchars($askedPage);
        echo <<<FIN
        <div class="card" style="max-width: 18rem;">
        <h5  class="card-header text-center">S'identifier</h5>
          <div class="card-body">
          <form action = "index.php?todo=login&page=$askedPagesafe" method = "post">
          <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="text" class="form-control" id="exampleInputEmail1"  name="email" value="$email" placeholder="Votre email sert d'identifiant" required/>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Mot de Passe</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-primary"> Se connecter </button>
        </form>
        <a href="index.php?page=register&todo=register">Créer un compte !</a>
        </div>
        </div>
        
    FIN;
    }

}

if (!function_exists('printLogOutForm')) {

    function printLogOutForm($askedPage) {
        echo <<<FIN
               <br>
              <form action = "index.php?todo=logout&page=$askedPage" method = "post">
                <button class="btn btn-danger" aria-pressed="true">Déconnexion</button>
              </form>           
        </div>
        </div>
        FIN;
    }

}

if (!function_exists('printRegisterForm')) {

    function printRegisterForm() {
        if (isset($_POST["nom"])) {
            $nom = htmlspecialchars($_POST["nom"]);
        } else {
            $nom = "";
        }
        if (isset($_POST["prenom"])) {
            $prenom = htmlspecialchars($_POST["prenom"]);
        } else {
            $prenom = "";
        }
        if (isset($_POST["naissance"])) {
            $naissance = $_POST["naissance"];
        } else {
            $naissance = "";
        }
        if (isset($_POST["contact"])) {
            $contact = htmlspecialchars($_POST["contact"]);
        } else {
            $contact = "";
        }
        if (isset($_POST["descriptif"])) {
            $descriptif = htmlspecialchars($_POST["descriptif"]);
        } else {
            $descriptif = "";
        }
        if (isset($_POST["email"])) {
            $email = $_POST["email"];
        } else {
            $email = "";
        }
        echo <<<FIN
            <div class="card text-center">
            <h5  class="card-header text-center"> Remplissez le formulaire d'inscription </h5>
            <div class="card-body">
            <form action="index.php?todo=register&page=register" method = post enctype="multipart/form-data" oninput = "up2.setCustomValidity(up2.value != up.value ? 'Les mots de passe diffèrent.' : '')">
            
            <div class="form-group">
            <label for="1">Adresse e-mail</label>
            <input type="email" class="form-control" id="1" name="email" placeholder="Votre email servira d'identifiant sur ce site" required value=$email>
            </div>
                
            <div class="form-group">
            <label for="2">Nom</label>
            <input type="text" class="form-control" id="2" name="nom" placeholder="Nom" required value=$nom>
            </div>
                
            <div class="form-group">
            <label for="3">Prénom</label>
            <input type="text" class="form-control" id="3"  name="prenom" placeholder="Prenom" required value=$prenom>
            </div>
    
            <div class="form-group">
            <label for="5">Date de naissance</label>
            <input type="date" class="form-control" id="5" name="naissance" required value=$naissance>
            </div>
            
                
            <div class="form-group">
            <label for="7">Mot de passe</label>
            <input type="password" class="form-control" id="7" name="password1" required>
            </div>
                
            <div class="form-group">
            <label for="8">Confirmez votre mot de passe</label>
            <input type="password" class="form-control" id="8"  name="password2" required>
            </div>
   
            <div class="form-group">
            <label>Un moyen de vous contacter directement</label>
            <textarea class="form-control"  rows="3" name="contact">$contact</textarea>
            <small class="form-text text-muted">Les utilisateurs interessés utiliseront ce moyen pour rentrer en contact avec vous ! C'est plus simple et rapide pour tout le monde.<</small>
            </div>
                
            <div class="form-group">
            <label >Que faites-vous sur le plateau ?</label>
            <textarea class="form-control"  rows="3" name="descriptif">$descriptif</textarea>
            <small  class="form-text text-muted">Exemple : "étudiant à l'ENSTA", "professeure à Polytechnique" ... Ce petit descriptif sera visible par tous.</small>
            </div>
                
            <div class="form-group">
            <label >Une petite photo ?</label>
            <input type="file" name="photo"/>
            </div>
                
            <button type="submit" class="btn btn-primary">Créer le compte</button>
            </form>
            </div>
            </div>
        FIN;
    }

}

if (!function_exists('printChangePasswordForm')) {

    function printChangePasswordForm() {
        if (isset($_POST["email"])) {
            $login = $_POST["email"];
        } else {
            $login = "";
        }
        echo <<<FIN
        <div class="card text-center">
        <h5 class="card-header">Si vous souhaitez changer votre mot de passe</h5>
        <div class="card-body">
        <form action="index.php?todo=changePassword&page=changePassword" method = post oninput = "up2.setCustomValidity(up2.value != up.value ? 'Les mots de passe diffèrent.' : '')">
        <div class="form-group">
            <label for="1">Adresse e-mail</label>
            <input type="email" class="form-control" id="1"  name="email" placeholder="Adresse e-mail" required value="$login">
        </div>
        <div class="form-group">
            <label for="7">Mot de passe actuel</label>
            <input type="password" class="form-control" id="7"  name="oldpassword" required>
            </div>
        <div class="form-group">
            <label for="8">Nouveau mot de passe</label>
            <input type="password" class="form-control" id="8"  name="password1" required>
        </div>
        <div class="form-group">
            <label for="9">Confirmez le nouveau mot de passe</label>
            <input type="password" class="form-control" id="9"  name="password2" required>
            </div>
        <button type="submit" class="btn btn-primary">Changer de mot de passe</button>
        </form>
        </div>
        </div>
      FIN;
    }

}

if (!function_exists('printDeleteUserForm')) {

    function printDeleteUserForm() {
        if (isset($_POST["login"])) {
            $login = $_POST["login"];
        } else {
            $login = "";
        }
        echo <<<FIN
        <div class="card text-center">
        <h5 class="card-header"> Si vous souhaitez supprimer votre compte</h5>
        <div class="card-body">
        <form action = "index.php?todo=deleteUser&page=deleteUser" method = "post">
        <div class="form-group">
            <label for="1">Adresse e-mail</label>
            <input type="text" class="form-control" id="1" name="email" placeholder="Adresse e-mail" required value="$login">
        </div>
        <div class="form-group">
            <label for="7">Mot de passe</label>
            <input type="password" class="form-control" id="7" name="password" required>
            </div>
        <button type="submit" class="btn btn-primary">Supprimer votre compte</button>
        </form>
        </div>
        </div>
        FIN;
    }

}

if (!function_exists('printRecTrajetForm')) {

    function printRecTrajetForm() {
        if (isset($_POST["depart"])){
            $depart=htmlspecialchars($_POST["depart"]);
        }else{
            $depart=null;
        }
        if (isset($_POST["arrivee"])){
            $arrivee=htmlspecialchars($_POST["arrivee"]);
        }else{
            $arrivee=null;
        }
        if (isset($_POST["date"])){
            $date=$_POST["date"];
        }else{
            $date=date('Y-m-d');
        }
        if (isset($_POST["debut"])){
            $debut=$_POST["debut"];
        }else{
            $debut=date('H:i');
        }
        if (isset($_POST["fin"])){
            $fin=$_POST["fin"];
        }else{
            $fin=null;
        }
        if (isset($_POST["infosupp"])){
            $infosupp=htmlspecialchars($_POST["infosupp"]);
        }else{
            $infosupp=null;
        }
        echo <<< FIN
    <div class="card text-center">
    <h5 class="card-header"> Vous souhaitez trouver un voyage correspondant à vos critères ? </h5>
    <div class="card-body">
    <form action='index.php?page=trajetrecherche&todo=recherche' method="post">
    <div class="form-group">
    <label >Lieu de départ</label>
    <input type=text class="form-control" id="depart" name="depart" placeholder="Soyez précis (par défaut : Ecole Polytechnique)" value="$depart" required  />
    </div>
    <p class="attention"> Attention, suivez les instructions suivantes pour remplir correctement le formulaire !</p>
    <p class="gras">1 - Remplissez précisément le champ du lieu puis <button class="btn btn-info" id="btn-geodepart"> Chercher ce lieu</button> </p>
    <div class="coordonnees">
        
        <input type=hidden class="form-control" id="latitude_dep" name="latitude_dep" required  />
        <input type=hidden class="form-control" id="longitude_dep" name="longitude_dep" required  />
    </div>
    <br>
    <div id="bouton_dep">
    <p class="gras">2 - Visualisez votre lieu de départ avec <button id="btn-carte_dep" class="btn btn-secondary">Afficher la carte</button></p> 
    </div> 
    <div class = "row" id="carte_dep">
        <br>
        <p class="gras">3 - Cliquez sur la carte pour placer un nouveau marqueur !</p>
        <p class="gras">Si vous éloignez le marqueur du lieu initial, complétez le champ avec plus d'infos sans recliquer sur les boutons.</p>
        <div class="col-md-12 col-md-offset-3" id="map-canvas_dep" style="height:400px"></div>
    </div>
    
    <div class="form-group">
        <label>Lieu d'arrivée</label>
        <input type=text class="form-control" id="arrivee" name="arrivee" placeholder="Soyez précis (par défaut : Ecole Polytechnique)" value="$arrivee" required/>
    </div>
    <button class="btn btn-info" id="btn-geoarrivee">1 - Chercher ce lieu</button>
    <div class="coordonnees">
        
        <input type=hidden class="form-control" id="latitude_arr" name="latitude_arr" required  />
        <input type=hidden class="form-control" id="longitude_arr" name="longitude_arr" required  />
    </div>
    <br>
    <div id="bouton_arr">
    <button id="btn-carte_arr" class="btn btn-secondary">2 - Afficher la carte</button>
    </div>
    <div class="row" id ="carte_arr">
        <br>
        <p class='centre'> <span style="text-decoration:underline">Cliquez sur la carte pour placer un nouveau marqueur !</span></p>
        <div class="col-md-12 col-md-offset-3" id="map-canvas_arr" style="height:400px"> Découvrez votre lieu d'arrivée sur la carte</div>
    </div>
    <div class="form-group">
        <label for="3">Date</label>
        <input type=date class="form-control" id="3" name="date" value="$date" required />
    </div>
    <div class="form-group">
        <label for="4">Heure de départ</label>
        <input type=time class="form-control" id="4" name="debut" value="$debut" required  />
    </div>
    <div class="form-group">
        <label for="5">Heure d'arrivée souhaitée à votre destination</label>
        <input type=time class="form-control" id="5" name="fin" value="$fin" required  />
    </div>
    
    <div class="form-group">
        <label for="6">Catégorie : </label>
        <input type=radio id="6" name="categorie" value="urgent" required/> Urgent <input type=radio name="categorie" value="important" required/> Important <input type=radio name="categorie" value="normal" required > Normal
    </div>
    <div class="form-group">
        <label>Veuillez indiquer les jours où vous effectuez ce trajet.</label><br>
        <p class='centre'><input type="checkbox" name="lundi_rec" value="1"> Lundi </p>
        <p class='centre'><input type="checkbox" name="mardi_rec" value="1"> Mardi </p> 
        <p class='centre'><input type="checkbox" name="mercredi_rec" value="1"> Mercredi </p>
        <p class='centre'><input type="checkbox" name="jeudi_rec" value="1"> Jeudi </p>
        <p class='centre'><input type="checkbox" name="vendredi_rec" value="1"> Vendredi </p> 
        <p class='centre'><input type="checkbox" name="samedi_rec" value="1"> Samedi </p> 
        <p class='centre'><input type="checkbox" name="dimanche_rec" value="1"> Dimanche </p> 
    </div> 
    
    <div class="form-group">
        <label for="8">Informations supplémentaires ?</label>
        <textarea class="form-control" id="8" name="infosupp" rows="5" cols="40">$infosupp</textarea>
        <small class="form-text text-muted">Indiquez des conditions de trajet particulières ... Ce petit descriptif sera visible par les utilisateurs du site.</small>
    </div>
    
    <button type="submit" class="btn btn-primary">Enregistrer ce trajet</button>
    </form>
    </div>
    </div>
    FIN;
    }

}

if (!function_exists('printProTrajetForm')) {

    function printProTrajetForm() {
        if (isset($_POST["depart"])){
            $depart=htmlspecialchars($_POST["depart"]);
        }else{
            $depart=null;
        }
        if (isset($_POST["arrivee"])){
            $arrivee=htmlspecialchars($_POST["arrivee"]);
        }else{
            $arrivee=null;
        }
        if (isset($_POST["date"])){
            $date=$_POST["date"];
        }else{
            $date=date('Y-m-d');
        }
        if (isset($_POST["debut"])){
            $debut=$_POST["debut"];
        }else{
            $debut=date('H:i');
        }
        if (isset($_POST["fin"])){
            $fin=$_POST["fin"];
        }else{
            $fin=null;
        }
        if (isset($_POST["places"])){
            $places=$_POST["places"];
        }else{
            $places=0;
        }
        if (isset($_POST["infosupp"])){
            $infosupp=htmlspecialchars($_POST["infosupp"]);
        }else{
            $infosupp=null;
        }
        echo <<< FIN
    <div class="card text-center">
    <h5 class="card-header"> Vous souhaitez proposer un voyage selon vos critères ? </h5>
    <div class="card-body">
    <form action='index.php?page=trajetpropose&todo=propose' method="post">
        
    <div class="form-group">
    <label>Lieu de départ</label>
    
    <input type=text class="form-control" id="depart" name="depart" placeholder="Soyez précis (par défaut : Ecole Polytechnique)" value="$depart" required/>
    </div> 
    <p class="attention"> Attention, suivez les instructions suivantes pour remplir correctement le formulaire !</p>
    <p class="gras">1 - Remplissez précisément le champ du lieu puis <button class="btn btn-info" id="btn-geodepart">Chercher ce lieu</button></p>
    <div class="coordonnees">
        <input type=hidden class="form-control" id="latitude_dep" name="latitude_dep" required  />
        <input type=hidden class="form-control" id="longitude_dep" name="longitude_dep" required  />
    </div>
    <br>
    <div id="bouton_dep">            
    <p class="gras">2 - Visualisez votre lieu de départ avec <button id="btn-carte_dep" class="btn btn-secondary">Afficher la carte</button></p>
    </div>
    <div class="row" id ="carte_dep">
        <br>
        <p class="gras">3 - Cliquez sur la carte pour placer un nouveau marqueur !</p>
        <p class="gras">Si vous éloignez le marqueur du lieu initial, complétez le champ avec plus d'infos sans recliquer sur les boutons.</p>
        <div class="col-md-12 col-md-offset-3" id="map-canvas_dep" style="height:400px"> Découvrez votre lieu de départ sur la carte</div>
    </div>
    <div class="form-group">
        <label>Lieu d'arrivée</label>
        <input type=text class="form-control" id="arrivee" name="arrivee" placeholder="Soyez précis (par défaut : Ecole Polytechnique)" value="$arrivee" required/>
    </div>
    <button class="btn btn-info" id="btn-geoarrivee">1 - Chercher ce lieu</button>
    <div class="coordonnees">
        <input type=hidden class="form-control" id="latitude_arr" name="latitude_arr" required  />
        <input type=hidden class="form-control" id="longitude_arr" name="longitude_arr" required  />
    </div>
    <br>
    <div id="bouton_arr">
    <button id="btn-carte_arr" class="btn btn-secondary">2 - Afficher la carte</button>
    </div>
    <div class="row" id ="carte_arr">
        <br>
        <p class='centre'> <span style="text-decoration:underline">Cliquez sur la carte pour placer un nouveau marqueur !</span></p>
        <div class="col-md-12 col-md-offset-3" id="map-canvas_arr" style="height:400px"> Découvrez votre lieu d'arrivée sur la carte</div>
    </div>
    <div class="form-group">
        <label for="3">Date</label>
        <input type=date class="form-control" id="3" name="date" required value="$date" />
    </div>
    <div class="form-group">
        <label for="4">Heure de départ</label>
        <input type=time class="form-control" id="4" name="debut" required value="$debut" />
    </div>
    <div class="form-group">
        <label for="5">Heure d'arrivée souhaitée à votre destination</label>
        <input type=time class="form-control" id="5" name="fin" required value="$fin" />
    </div>
    <div class="form-group">
        <label for="6">Combiez de places avez-vous à disposition ? </label>
        <input type=number class="form-control" id="6" name="places" required value="$places"/>
    </div>
    <div class="form-group">
        <label>Veuillez indiquer les jours où vous effectuez ce trajet.</label><br>
        <p class='centre'><input type="checkbox" name="lundi_pro" value="1"> Lundi </p>
        <p class='centre'><input type="checkbox" name="mardi_pro" value="1"> Mardi </p> 
        <p class='centre'><input type="checkbox" name="mercredi_pro" value="1"> Mercredi </p>
        <p class='centre'><input type="checkbox" name="jeudi_pro" value="1"> Jeudi </p>
        <p class='centre'><input type="checkbox" name="vendredi_pro" value="1"> Vendredi </p> 
        <p class='centre'><input type="checkbox" name="samedi_pro" value="1"> Samedi </p> 
        <p class='centre'><input type="checkbox" name="dimanche_pro" value="1"> Dimanche </p> 
    </div> 
    <div class="form-group">
        <label for="8">Informations supplémentaires ?</label>
        <textarea class="form-control" id="8" name="infosupp" rows="5" cols="40" >$infosupp</textarea>
        <small  class="form-text text-muted">Indiquez des conditions de trajet particulières ... Ce petit descriptif sera visible par les utilisateurs du site.</small>
    </div>
    
    <div class="form-group" id="etapes">
        <label for="9">Etapes possibles </label>
        <input id="9" class="form-control" type=text name="etapes[]"/><br/> 
    </div> 
                
    <div id="validation">
        <button type="button" id="ajoutTag" class="btn btn-success"> Rajouter une étape</button>
        <button type="submit" class="btn btn-primary">Enregistrer ce trajet</button> 
    </div>
    </form>
    </div>
    </div>
    FIN;
    }

}

if (!function_exists('printModifierInfoForm')) {

    function printModifierInfoForm($user) {
        if (isset($_POST["nom"])) {
            $nom = htmlspecialchars($_POST["nom"]);
        } else {
            $nom = $user->nom;
        }
        if (isset($_POST["prenom"])) {
            $prenom = htmlspecialchars($_POST["prenom"]);
        } else {
            $prenom = $user->prenom;
        }
        if (isset($_POST["naissance"])) {
            $naissance = $_POST["naissance"];
        } else {
            $naissance = $user->naissance;
        }
        if (isset($_POST["contact"])) {
            $contact = htmlspecialchars($_POST["contact"]);
        } else {
            $contact = $user->contact;
        }
        if (isset($_POST["descriptif"])) {
            $descriptif = htmlspecialchars($_POST["descriptif"]);
        } else {
            $descriptif = $user->descriptif;
        }
        echo <<<FIN
            <div class="card text-center">
            <h5 class="card-header"> Si vous souhaitez modifier votre profil </h5>
            <div class="card-body">
            <form action="index.php?todo=modifierinfos&page=modifierinfos" method = post enctype="multipart/form-data" oninput = "up2.setCustomValidity(up2.value != up.value ? 'Les mots de passe diffèrent.' : '')">
    
            <div class="form-group">
            <label for="2">Nom</label>
            <input type="text" class="form-control" id="2"  name="nom" placeholder="Nom" required value=$nom>
            </div>
                
            <div class="form-group">
            <label for="3">Prénom</label>
            <input type="text" class="form-control" id="3"  name="prenom" placeholder="Prenom" required value=$prenom>
            </div>
    
            <div class="form-group">
            <label for="5">Date de naissance</label>
            <input type="date" class="form-control" id="5" name="naissance" required value=$naissance>
            </div>
           

            <div class="form-group">
            <label >Un moyen de vous contacter directement</label>
            <textarea class="form-control" rows="3" name="contact">$contact</textarea>
            <small class="form-text text-muted">Les utilisateurs interessés utiliseront ce moyen pour rentrer en contact avec vous ! C'est plus simple et rapide pour tout le monde.</small>
            </div>
                
            <div class="form-group">
            <label>Que faites-vous sur le plateau ?</label>
            <textarea class="form-control"  rows="3" name="descriptif">$descriptif</textarea>
            <small class="form-text text-muted">Exemple : "étudiant à l'ENSTA", "professeure à Polytechnique" ... Ce petit descriptif sera visible par tous.</small>
            </div>
                  
            <div class="form-group">
            <label for="7">Mot de passe</label>
            <input type="password" class="form-control" id="7" name="password1" required>
            </div>
                
            <div class="form-group">
            <label>Photo de profil</label>
            <input type="file" name="photo"/>
            </div>
                
            <button type="submit" class="btn btn-primary">Modifier !</button>
            </form>
            </div>
            </div>
        FIN;
    }

}

if (!function_exists('printModifierProTrajetForm')) {

    function printModifierProTrajetForm($trajet) {
        
        if (isset($_POST["depart"])){
            $depart=htmlspecialchars($_POST["depart"]);
        }else{
            $depart=$trajet->depart;
        }
        if (isset($_POST["latitude_dep"])){
            $latitude_dep=$_POST["latitude_dep"];
        }else{
            $latitude_dep=$trajet->latitude_dep;
        }
        if (isset($_POST["longitude_dep"])){
            $longitude_dep=$_POST["longitude_dep"];
        }else{
            $longitude_dep=$trajet->longitude_dep;
        }
        if (isset($_POST["arrivee"])){
            $arrivee=htmlspecialchars($_POST["arrivee"]);
        }else{
            $arrivee=$trajet->arrivee;
        }
        if (isset($_POST["latitude_arr"])){
            $latitude_arr=$_POST["latitude_arr"];
        }else{
            $latitude_arr=$trajet->latitude_arr;
        }
        if (isset($_POST["longitude_arr"])){
            $longitude_arr=$_POST["longitude_arr"];
        }else{
            $longitude_arr=$trajet->longitude_arr;
        }
        if (isset($_POST["date"])){
            $date=$_POST["date"];
        }else{
            $date=$trajet->date;
        }
        if (isset($_POST["debut"])){
            $debut=$_POST["debut"];
        }else{
            $debut=$trajet->debut;
        }
        if (isset($_POST["fin"])){
            $fin=$_POST["fin"];
        }else{
            $fin=$trajet->fin;
        }
        if (isset($_POST["places"])){
            $places=$_POST["places"];
        }else{
            $places=$trajet->places;
        }
        if (isset($_POST["infosupp"])){
            $infosupp=htmlspecialchars($_POST["infosupp"]);
        }else{
            $infosupp=$trajet->infosupp;
        }
        if (isset($_POST["etapes[]"])){
            $etapes=$_POST["etapes[]"];
        }else{
            $etapes=$trajet->etapes;
        }
        echo <<< FIN
    <div class="card text-center">
    <h5 class="card-header"> Modifier votre proposition de trajet </h5>
    <div class="card-body">
    <form action='index.php?page=trajetpropose&todo=propose' method="post">
        
    <div class="form-group">
    <label for="1">Lieu de départ</label>
    <input type=text class="form-control" id="depart" name="depart" placeholder="Soyez précis (par défaut : Ecole Polytechnique)" value="$depart" required  />
    </div> 
    <button class="btn btn-info" id="btn-geodepart">1- Chercher ce lieu</button>
    <div class="coordonnees">
        
        <input type=hidden class="form-control" id="latitude_dep" name="latitude_dep" value=$latitude_dep required  />
        <input type=hidden class="form-control" id="longitude_dep" name="longitude_dep" value=$longitude_dep required  />
    </div>
    <br>
    <button id="btn-carte_dep" class="btn btn-secondary">2- Afficher la carte</button>
    <div class="row" id ="carte_dep">
        <br>
        <p class='centre'> <span style="text-decoration:underline">Cliquez sur la carte pour placer un nouveau marqueur !</span></p>
        <div class="col-md-12 col-md-offset-3" id="map-canvas_dep" style="height:400px"> Découvrez votre lieu de départ sur la carte</div>
    </div>
    <div class="form-group">
        <label for="2">Lieu d'arrivée</label>
        <input type=text class="form-control" id="arrivee" name="arrivee" placeholder="Soyez précis (par défaut : Ecole Polytechnique)" value="$arrivee" required value=$arrivee />
    </div>
    <button class="btn btn-info" id="btn-geoarrivee">1 - Chercher ce lieu</button>
    <div class="coordonnees">
        
        <input type=hidden class="form-control" id="latitude_arr" name="latitude_arr"  value=$latitude_arr required  />
        <input type=hidden class="form-control" id="longitude_arr" name="longitude_arr" value=$longitude_arr required  />
    </div>
    <br>
    <button id="btn-carte_arr" class="btn btn-secondary">2 - Afficher la carte</button>
    <div class="row" id ="carte_arr">
        <br>
        <p class='centre'> <span style="text-decoration:underline">Cliquez sur la carte pour placer un nouveau marqueur !</span></p>
        <div class="col-md-12 col-md-offset-3" id="map-canvas_arr" style="height:400px"> Découvrez votre lieu d'arrivée sur la carte</div>
    </div>
    <div class="form-group">
        <label for="3">Date</label>
        <input type=date class="form-control" id="3" name="date" required value="$date" />
    </div>
    <div class="form-group">
        <label for="4">Heure de départ</label>
        <input type=time class="form-control" id="4" name="debut" required value="$debut" />
    </div>
    <div class="form-group">
        <label for="5">Heure d'arrivée souhaitée à votre destination</label>
        <input type=time class="form-control" id="5" name="fin" required value="$fin" />
    </div>
    <div class="form-group">
        <label for="6">Combiez de places avez-vous à disposition ? </label>
        <input type=number class="form-control" id="6" name="places" required value="$places"/>
    </div>
    <div class="form-group">
        <label for="7">Veuillez indiquer les jours où vous effectuez ce trajet.</label><br>
        <p class='centre'><input type="checkbox" name="lundi_pro" value="1"> Lundi </p>
        <p class='centre'><input type="checkbox" name="mardi_pro" value="1"> Mardi </p> 
        <p class='centre'><input type="checkbox" name="mercredi_pro" value="1"> Mercredi </p>
        <p class='centre'><input type="checkbox" name="jeudi_pro" value="1"> Jeudi </p>
        <p class='centre'><input type="checkbox" name="vendredi_pro" value="1"> Vendredi </p> 
        <p class='centre'><input type="checkbox" name="samedi_pro" value="1"> Samedi </p> 
        <p class='centre'><input type="checkbox" name="dimanche_pro" value="1"> Dimanche </p> 
    </div> 
    <div class="form-group">
        <label for="8">Informations supplémentaires ?</label>
        <textarea class="form-control" id="8" name="infosupp" rows="5" cols="40">$infosupp</textarea>
        <small class="form-text text-muted">Indiquez des conditions de trajet particulières ... Ce petit descriptif sera visible par les utilisateurs du site.</small>
    </div>
    <div class="form-group" id="etapes">
        <label for="9">Etapes possibles </label>
        <input id="9" class="form-control" type=text name="etapes[]" value=$etapes><br/>
    </div> 
                
    <div id="validation">
        <button type="button" id="ajoutTag" class="btn btn-success"> Rajouter une étape</button>
        <button type="submit" class="btn btn-primary">Modifier la proposition</button> 
    </div>
    </form>
    </div>
    </div>
    FIN;
    }

}

if (!function_exists('printModifierRecTrajetForm')) {

    function printModifierRecTrajetForm($trajet) {
        if (isset($_POST["depart"])){
            $depart=htmlspecialchars($_POST["depart"]);
        }else{
            $depart=$trajet->depart;
        }
        if (isset($_POST["latitude_dep"])){
            $latitude_dep=$_POST["latitude_dep"];
        }else{
            $latitude_dep=$trajet->latitude_dep;
        }
        if (isset($_POST["longitude_dep"])){
            $longitude_dep=$_POST["longitude_dep"];
        }else{
            $longitude_dep=$trajet->longitude_dep;
        }
        if (isset($_POST["arrivee"])){
            $arrivee=htmlspecialchars($_POST["arrivee"]);
        }else{
            $arrivee=$trajet->arrivee;
        }
        if (isset($_POST["latitude_arr"])){
            $latitude_arr=$_POST["latitude_arr"];
        }else{
            $latitude_arr=$trajet->latitude_arr;
        }
        if (isset($_POST["longitude_arr"])){
            $longitude_arr=$_POST["longitude_arr"];
        }else{
            $longitude_arr=$trajet->longitude_arr;
        }
        if (isset($_POST["date"])){
            $date=$_POST["date"];
        }else{
            $date=$trajet->date;
        }
        if (isset($_POST["debut"])){
            $debut=$_POST["debut"];
        }else{
            $debut=$trajet->debut;
        }
        if (isset($_POST["fin"])){
            $fin=$_POST["fin"];
        }else{
            $fin=$trajet->fin;
        }
        if (isset($_POST["categorie"])){
            $places=$_POST["categorie"];
        }else{
            $categorie=$trajet->categorie;
        }
        if (isset($_POST["infosupp"])){
            $infosupp=htmlspecialchars($_POST["infosupp"]);
        }else{
            $infosupp=$trajet->infosupp;
        }
        
        echo <<< FIN
    <div class="card text-center">
    <h5 class="card-header"> Modifier votre recherche de trajet</h5>
    <div class="card-body">
    <form action='index.php?page=trajetrecherche&todo=recherche' method="post">
    <div class="form-group">
    <label for="1">Lieu de départ</label>
    <input type=text class="form-control" id="depart" name="depart" placeholder="Soyez précis (par défaut : Ecole Polytechnique)" value="$depart" required  />
    </div> 
    <button class="btn btn-info" id="btn-geodepart">1 - Chercher ce lieu</button>
    <div class="coordonnees">
        
        <input type=hidden class="form-control" id="latitude_dep" name="latitude_dep" value=$latitude_dep required  />
        <input type=hidden class="form-control" id="longitude_dep" name="longitude_dep" value=$longitude_dep required  />
    </div>
    <br>
    
    <button id="btn-carte_dep" class="btn btn-secondary">2 - Afficher la carte</button> 
    
    <div class = "row" id="carte_dep">
        <br>
        <p class='centre'> <span style="text-decoration:underline">Cliquez sur la carte pour placer un nouveau marqueur !</span></p>
        <div class="col-md-12 col-md-offset-3" id="map-canvas_dep" style="height:400px"></div>
    </div>
    <div class="form-group">
        <label for="2">Lieu d'arrivée</label>
        <input type=text class="form-control" id="arrivee" name="arrivee" placeholder="Soyez précis (par défaut : Ecole Polytechnique)" value="$arrivee" required value=$arrivee />
    </div>
    <button class="btn btn-info" id="btn-geoarrivee">1 - Chercher ce lieu</button>
    <div class="coordonnees">
        
        <input type=hidden class="form-control" id="latitude_arr" name="latitude_arr" value=$latitude_arr required  />
        <input type=hidden class="form-control" id="longitude_arr" name="longitude_arr" value=$longitude_arr required  />
    </div>
    <br>
    
    <button id="btn-carte_arr" class="btn btn-secondary">2 - Afficher la carte</button>
    
    <div class="row" id ="carte_arr">
        <br>
        <p class='centre'> <span style="text-decoration:underline">Cliquez sur la carte pour placer un nouveau marqueur !</span></p>
        <div class="col-md-12 col-md-offset-3" id="map-canvas_arr" style="height:400px"> Découvrez votre lieu d'arrivée sur la carte</div>
    </div>
    <div class="form-group">
        <label for="3">Date</label>
        <input type=date class="form-control" id="3" name="date" value="$date" required />
    </div>
    <div class="form-group">
        <label for="4">Heure de départ</label>
        <input type=time class="form-control" id="4" name="debut" value="$debut" required  />
    </div>
    <div class="form-group">
        <label for="5">Heure d'arrivée souhaitée à votre destination</label>
        <input type=time class="form-control" id="5" name="fin" value="$fin" required  />
    </div>
    
    <div class="form-group">
        <label for="6">Catégorie : </label>
        <input type=radio id="6" name="categorie" value="urgent"/> Urgent <input type=radio name="categorie" value="important"/> Important <input type=radio name="categorie" value="normal"> Normal
    </div>
    <div class="form-group">
        <label for="7">Veuillez indiquer les jours où vous effectuez ce trajet.</label><br>
        <p class='centre'><input type="checkbox" name="lundi_rec" value="1"> Lundi </p>
        <p class='centre'><input type="checkbox" name="mardi_rec" value="1"> Mardi </p> 
        <p class='centre'><input type="checkbox" name="mercredi_rec" value="1"> Mercredi </p>
        <p class='centre'><input type="checkbox" name="jeudi_rec" value="1"> Jeudi </p>
        <p class='centre'><input type="checkbox" name="vendredi_rec" value="1"> Vendredi </p> 
        <p class='centre'><input type="checkbox" name="samedi_rec" value="1"> Samedi </p> 
        <p class='centre'><input type="checkbox" name="dimanche_rec" value="1"> Dimanche </p> 
    </div> 
    
    <div class="form-group">
        <label for="8">Informations supplémentaires ?</label>
        <textarea class="form-control" id="8" name="infosupp" rows="5" cols="40" >$infosupp</textarea>
        <small class="form-text text-muted">Indiquez des conditions de trajet particulières ... Ce petit descriptif sera visible par les utilisateurs du site.</small>
    </div>
    
    <button type="submit" class="btn btn-primary">Enregistrer ce trajet</button>
    </form>
    </div>
    </div>
    FIN;
    }

}

if (!(function_exists('printAdminDeleteForm'))){
    
    function printAdminDeleteForm($dbh,$type,$objet){
        if ($type=='pro'){
            $id=$objet->voyagepro;
        }
        if ($type=='rec'){
            $id=$objet->voyagerec;
        }
        if ($type=='user'){
            $id=$objet->email;
        }
        
        echo <<<FIN
            <form action='index.php?page=zoneadmin&todo=delete&type=$type&id=$id' method="post">
                <div class="form-group">
                <label>Pourquoi supprimer ceci ?</label>
                <textarea class="form-control"  rows="3" name="raisons"></textarea>
                <small class="form-text text-muted">Ce justificatif sera envoyé par mail à l'intéressé(e).</small>
                </div>
                <button type="submit" class="btn btn-danger">Supprimer</button>
            </form>
        FIN;
        
    }
}

?>

