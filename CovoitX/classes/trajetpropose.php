<?php

class TrajetPropose {

    public $voyagepro;
    public $conducteur;
    public $depart;
    public $latitude_dep;
    public $longitude_dep;
    public $arrivee;
    public $latitude_arr;
    public $longitude_arr;
    public $debut;
    public $fin;
    public $date;
    public $places;
    public $etapes;
    public $lundi_pro;
    public $mardi_pro;
    public $mercredi_pro;
    public $jeudi_pro;
    public $vendredi_pro;
    public $samedi_pro;
    public $dimanche_pro;
    public $kilometrage;
    public $infosupp;
    
    function modifier($dbh, $depart,$latitude_dep,$longitude_dep, $arrivee,$latitude_arr,$longitude_arr, $debut, $fin, $date, $places, $lundi_pro,$mardi_pro,$mercredi_pro,$jeudi_pro,$vendredi_pro,$samedi_pro,$dimanche_pro, $etapes, $infosupp, $id){
        $sth = $dbh->prepare("UPDATE `trajetpropose` SET `depart`=?, `latitude_dep`=?, `longitude_dep`=?, `arrivee`=?, `latitude_arr`=?, `longitude_arr`=?, `debut`=?, `fin`=?, `date`=?, `places`=?,`lundi_pro`=?, `mardi_pro`=?, `mercredi_pro`=?, `jeudi_pro`=?, `vendredi_pro`=?, `samedi_pro`=?, `dimanche_pro`=?, `infosupp`=? WHERE `voyagepro`=?;");
        $sth->execute(array(htmlspecialchars($depart),$latitude_dep,$longitude_dep, htmlspecialchars($arrivee),$latitude_arr,$longitude_arr, $debut, $fin, $date, $places, $lundi_pro,$mardi_pro,$mercredi_pro,$jeudi_pro,$vendredi_pro,$samedi_pro,$dimanche_pro, htmlspecialchars($infosupp), $id));
        if ($etapes != null) {
            foreach ($etapes as $lieu) {
                if ($lieu!=""){
                $sth = $dbh->prepare("INSERT INTO `etapes` (`voyage`,`lieuetape`) VALUES (?,?)");
                $sth->execute(array($id, htmlspecialchars($lieu)));
                }
            }
        }
    }
    //vérification de la coherence avec les trajets acceptes------------------------------------------------------------------------------------------
    
    function Collision($dbh, $email, $debut, $fin, $date) {
        $sth1 = $dbh->prepare('SELECT * FROM `trajetaccepte` JOIN `trajetpropose` ON `client`=`conducteur` AND `idvoyage`=`voyagepro` WHERE `statut`="propose" AND `client`=? AND `date`=? AND NOT(`fin`<? OR `debut`>?);');
        $request1 = $sth1->execute(array($email, $date, $debut,$fin)); #a proposé un trajet accepté sur le même créneau
        $count1=$sth1 -> rowCount();
        $booleen1= ($count1>0);
        $sth2 = $dbh->prepare('SELECT * FROM `trajetaccepte` JOIN `trajetrecherche` ON `client`=`demandeur`AND`idvoyage`=`voyagerec` WHERE `statut`="recherche" AND `client`=? AND `date`=? AND NOT(`fin`<? OR `debut`>?);');
        $request2 = $sth2->execute(array($email, $date, $debut,$fin)); #a recherché un trajet accepté sur le même créneau
        $count2=$sth2 -> rowCount();
        $booleen2= ($count2>0);
        return($booleen1 || $booleen2);
    }
    
    function estAccepte($dbh,$email,$voyagepro){
        $sth=$dbh->prepare('SELECT * FROM `trajetaccepte` JOIN `trajetpropose` ON `client`=`conducteur` AND `idvoyage`=`voyagepro` WHERE `statut`="propose" AND `client`=? AND `idvoyage`=?;');
        $request=$sth->execute(array($email,$voyagepro));
        $count=$sth -> rowCount();
        return($count>0);
    }
    
    function accepterTrajet($dbh,$email,$id_voyage,$client){
        $sth = $dbh->prepare("INSERT INTO `trajetaccepte` (`idvoyage`,`client`,`statut`) VALUES (?,?,?);");
        $request=$sth->execute(array($id_voyage,$email,$client));
    }
    
    function refuserTrajet($dbh,$email,$id_voyage,$client){
        $sth = $dbh->prepare("DELETE FROM `trajetaccepte` WHERE `idvoyage`=? AND `client`=? AND`statut`=?;");
        $request=$sth->execute(array($id_voyage,$email,$client));
    }
    
    //MATCHING-------------------------------------------------------------------------------------------------------------------------------------------------
    
    function getAllMatching1($dbh,$id_voyage){
        $voyage= TrajetPropose::getTrajetPro($dbh,$id_voyage);
        $latitude_dep=$voyage->latitude_dep;
        $longitude_dep=$voyage->longitude_dep;
        $latitude_arr=$voyage->latitude_arr;
        $longitude_arr=$voyage->longitude_arr;
        $debut=$voyage->debut;
        $user=$voyage->conducteur;
        $date=date_create_from_format('Y-m-d',$voyage->date);
        $annee=date_format($date, 'Y');
        $mois=date_format($date, 'm');
        $journee=date_format($date, 'd');
        $jour=date('w', mktime(0,0,0,$mois,$journee,$annee));
        //get_distance_metres a été ajouté sur sql via phpmyadmin
        $trajets_total=array();
        $request_total=0;
        if ($voyage->dimanche_pro==1 || $jour==0 ){
        $sth=$dbh->prepare('SELECT *,get_distance_metres(?,?,`latitude_dep`, `longitude_dep`) AS `distance`,ABS(TIMEDIFF(?, `debut`)) AS `diff` FROM `trajetrecherche` WHERE (`demandeur`<>?) AND (get_distance_metres(?,?,`latitude_dep`, `longitude_dep`)<1000) AND (get_distance_metres(?,?,`latitude_arr`, `longitude_arr`)<5000) AND (ABS(TIMEDIFF(?, `debut`)) < "10001") AND (`date`=? OR `dimanche_rec`=1 OR DAYOFWEEK(`date`)=1) ORDER BY `distance` ASC, `diff` ASC;');
        $request=$sth->execute(array($latitude_dep,$longitude_dep,$debut,$user,$latitude_dep,$longitude_dep,$latitude_arr,$longitude_arr,$debut,$voyage->date));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetRecherche');
        $request_succeeded = $sth->execute();
        if ($request_succeeded){
            $trajets = $sth->fetchAll();
            $sth->closeCursor();
            $trajets_total=array_merge($trajets_total,$trajets);
            $request_total=$request_total+1;
        }
        
        }
        if ( $voyage->lundi_pro==1|| $jour==1){
        $sth=$dbh->prepare('SELECT *,get_distance_metres(?,?,`latitude_dep`, `longitude_dep`) AS `distance`,ABS(TIMEDIFF(?, `debut`)) AS `diff` FROM `trajetrecherche` WHERE (`demandeur`<>?) AND(get_distance_metres(?,?,`latitude_dep`, `longitude_dep`)<1000) AND (get_distance_metres(?,?,`latitude_arr`, `longitude_arr`)<5000) AND (ABS(TIMEDIFF(?, `debut`)) < "10001") AND (`date`=? OR `lundi_rec`=1 OR DAYOFWEEK(`date`)=2) ORDER BY `distance` ASC, `diff` ASC;');
        $request=$sth->execute(array($latitude_dep,$longitude_dep,$debut,$user,$latitude_dep,$longitude_dep,$latitude_arr,$longitude_arr,$debut,$voyage->date));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetRecherche');
        $request_succeeded = $sth->execute();
        if ($request_succeeded){
            $trajets = $sth->fetchAll();
            $sth->closeCursor();
            $trajets_total=array_merge($trajets_total,$trajets);
            $request_total=$request_total+1;
        }
        
        }
        if (  $voyage->mardi_pro==1 || $jour==2){
        $sth=$dbh->prepare('SELECT *,get_distance_metres(?,?,`latitude_dep`, `longitude_dep`) AS `distance`,ABS(TIMEDIFF(?, `debut`)) AS `diff` FROM `trajetrecherche` WHERE (`demandeur`<>?) AND (get_distance_metres(?,?,`latitude_dep`, `longitude_dep`)<1000) AND (get_distance_metres(?,?,`latitude_arr`, `longitude_arr`)<5000) AND (ABS(TIMEDIFF(?, `debut`)) < "10001") AND (`date`=? OR `mardi_rec`=1 OR DAYOFWEEK(`date`)=3) ORDER BY `distance` ASC, `diff` ASC;');
        $request=$sth->execute(array($latitude_dep,$longitude_dep,$debut,$user,$latitude_dep,$longitude_dep,$latitude_arr,$longitude_arr,$debut,$voyage->date));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetRecherche');
        $request_succeeded = $sth->execute();
        if ($request_succeeded){
            $trajets = $sth->fetchAll();
            $sth->closeCursor();
            $trajets_total=array_merge($trajets_total,$trajets);
            $request_total=$request_total+1;
        }
        
        }
        if (  $voyage->mercredi_pro==1 || $jour==3){
        $sth=$dbh->prepare('SELECT *,get_distance_metres(?,?,`latitude_dep`, `longitude_dep`) AS `distance`,ABS(TIMEDIFF(?, `debut`)) AS `diff` FROM `trajetrecherche` WHERE (`demandeur`<>?) AND (get_distance_metres(?,?,`latitude_dep`, `longitude_dep`)<1000) AND (get_distance_metres(?,?,`latitude_arr`, `longitude_arr`)<5000) AND (ABS(TIMEDIFF(?, `debut`)) < "10001") AND (`date`=? OR `mercredi_rec`=1 OR DAYOFWEEK(`date`)=4) ORDER BY `distance` ASC, `diff` ASC;');
        $request=$sth->execute(array($latitude_dep,$longitude_dep,$debut,$user,$latitude_dep,$longitude_dep,$latitude_arr,$longitude_arr,$debut,$voyage->date));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetRecherche');
        $request_succeeded = $sth->execute();
        if ($request_succeeded){
            $trajets = $sth->fetchAll();
            $sth->closeCursor();
            $trajets_total=array_merge($trajets_total,$trajets);
            $request_total=$request_total+1;
        }
        
        }
        if ( $voyage->jeudi_pro==1 || $jour==4){
        $sth=$dbh->prepare('SELECT *,get_distance_metres(?,?,`latitude_dep`, `longitude_dep`) AS `distance`,ABS(TIMEDIFF(?, `debut`)) AS `diff` FROM `trajetrecherche` WHERE (`demandeur`<>?) AND (get_distance_metres(?,?,`latitude_dep`, `longitude_dep`)<1000) AND (get_distance_metres(?,?,`latitude_arr`, `longitude_arr`)<5000) AND (ABS(TIMEDIFF(?, `debut`)) < "10001") AND (`date`=? OR `jeudi_rec`=1 OR DAYOFWEEK(`date`)=5) ORDER BY `distance` ASC, `diff` ASC;');
        $request=$sth->execute(array($latitude_dep,$longitude_dep,$debut,$user,$latitude_dep,$longitude_dep,$latitude_arr,$longitude_arr,$debut,$voyage->date));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetRecherche');
        $request_succeeded = $sth->execute();
        if ($request_succeeded){
            $trajets = $sth->fetchAll();
            $sth->closeCursor();
            $trajets_total=array_merge($trajets_total,$trajets);
            $request_total=$request_total+1;
        }
        
        }
        if ( $voyage->vendredi_pro==1 || $jour==5){
        $sth=$dbh->prepare('SELECT *,get_distance_metres(?,?,`latitude_dep`, `longitude_dep`) AS `distance`,ABS(TIMEDIFF(?, `debut`)) AS `diff` FROM `trajetrecherche` WHERE (`demandeur`<>?) AND (get_distance_metres(?,?,`latitude_dep`, `longitude_dep`)<1000) AND (get_distance_metres(?,?,`latitude_arr`, `longitude_arr`)<5000) AND (ABS(TIMEDIFF(?, `debut`)) < "10001") AND (`date`=? OR `vendredi_rec`=1 OR DAYOFWEEK(`date`)=6) ORDER BY `distance` ASC, `diff` ASC;');
        $request=$sth->execute(array($latitude_dep,$longitude_dep,$debut,$user,$latitude_dep,$longitude_dep,$latitude_arr,$longitude_arr,$debut,$voyage->date));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetRecherche');
        $request_succeeded = $sth->execute();
        if ($request_succeeded){
            $trajets = $sth->fetchAll();
            $sth->closeCursor();
            $trajets_total=array_merge($trajets_total,$trajets);
            $request_total=$request_total+1;
        }
        
        }
        if ( $voyage->samedi_pro==1 || $jour==6){
        $sth=$dbh->prepare('SELECT *,get_distance_metres(?,?,`latitude_dep`, `longitude_dep`) AS `distance`,ABS(TIMEDIFF(?, `debut`)) AS `diff` FROM `trajetrecherche` WHERE (`demandeur`<>?) AND (get_distance_metres(?,?,`latitude_dep`, `longitude_dep`)<1000) AND (get_distance_metres(?,?,`latitude_arr`, `longitude_arr`)<5000) AND (ABS(TIMEDIFF(?, `debut`)) < "10001") AND (`date`=? OR `samedi_rec`=1 OR DAYOFWEEK(`date`)=7) ORDER BY `distance` ASC, `diff` ASC;');
        $request=$sth->execute(array($latitude_dep,$longitude_dep,$debut,$user,$latitude_dep,$longitude_dep,$latitude_arr,$longitude_arr,$debut,$voyage->date));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetRecherche');
        $request_succeeded = $sth->execute();
        if ($request_succeeded){
            $trajets = $sth->fetchAll();
            $sth->closeCursor();
            $trajets_total=array_merge($trajets_total,$trajets);
            $request_total=$request_total+1;
        }
        
        }
        
        if ($request_total>0) {
            return array_unique($trajets_total,SORT_REGULAR);
        } else {
            return NULL;
        }
    }
    
    function getAllMatching2($dbh,$id_voyage){
        $voyage= TrajetPropose::getTrajetPro($dbh,$id_voyage);
        $latitude_dep=$voyage->latitude_dep;
        $longitude_dep=$voyage->longitude_dep;
        $latitude_arr=$voyage->latitude_arr;
        $longitude_arr=$voyage->longitude_arr;
        $debut=$voyage->debut;
        $user=$voyage->conducteur;
        $date=date_create_from_format('Y-m-d',$voyage->date);
        $annee=date_format($date, 'Y');
        $mois=date_format($date, 'm');
        $journee=date_format($date, 'd');
        $jour=date('w', mktime(0,0,0,$mois,$journee,$annee));
        //get_distance_metres a été ajouté sur sql via phpmyadmin
        $trajets_total=array();
        $request_total=0;
        if ($voyage->dimanche_pro==1 || $jour==0 ){
        $sth=$dbh->prepare('SELECT *,get_distance_metres(?,?,`latitude_dep`, `longitude_dep`) AS `distance`,ABS(TIMEDIFF(?, `debut`)) AS `diff` FROM `trajetrecherche` WHERE (`demandeur`<>?) AND (get_distance_metres(?,?,`latitude_dep`, `longitude_dep`)<2000) AND (get_distance_metres(?,?,`latitude_arr`, `longitude_arr`)<6000) AND (ABS(TIMEDIFF(?, `debut`)) < "20001") AND (`date`=? OR `dimanche_rec`=1 OR DAYOFWEEK(`date`)=1) ORDER BY `distance` ASC, `diff` ASC;');
        $request=$sth->execute(array($latitude_dep,$longitude_dep,$debut,$user,$latitude_dep,$longitude_dep,$latitude_arr,$longitude_arr,$debut,$voyage->date));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetRecherche');
        $request_succeeded = $sth->execute();
        if ($request_succeeded){
            $trajets = $sth->fetchAll();
            $sth->closeCursor();
            $trajets_total=array_merge($trajets_total,$trajets);
            $request_total=$request_total+1;
        }
        
        }
        if ( $voyage->lundi_pro==1|| $jour==1){
        $sth=$dbh->prepare('SELECT *,get_distance_metres(?,?,`latitude_dep`, `longitude_dep`) AS `distance`,ABS(TIMEDIFF(?, `debut`)) AS `diff` FROM `trajetrecherche` WHERE (`demandeur`<>?) AND (get_distance_metres(?,?,`latitude_dep`, `longitude_dep`)<2000) AND (get_distance_metres(?,?,`latitude_arr`, `longitude_arr`)<6000) AND (ABS(TIMEDIFF(?, `debut`)) < "20001") AND (`date`=? OR `lundi_rec`=1 OR DAYOFWEEK(`date`)=2) ORDER BY `distance` ASC, `diff` ASC;');
        $request=$sth->execute(array($latitude_dep,$longitude_dep,$debut,$user, $latitude_dep,$longitude_dep,$latitude_arr,$longitude_arr,$debut,$voyage->date));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetRecherche');
        $request_succeeded = $sth->execute();
        if ($request_succeeded){
            $trajets = $sth->fetchAll();
            $sth->closeCursor();
            $trajets_total=array_merge($trajets_total,$trajets);
            $request_total=$request_total+1;
        }
        
        }
        if (  $voyage->mardi_pro==1 || $jour==2){
        $sth=$dbh->prepare('SELECT *,get_distance_metres(?,?,`latitude_dep`, `longitude_dep`) AS `distance`,ABS(TIMEDIFF(?, `debut`)) AS `diff` FROM `trajetrecherche` WHERE (`demandeur`<>?) AND (get_distance_metres(?,?,`latitude_dep`, `longitude_dep`)<2000) AND (get_distance_metres(?,?,`latitude_arr`, `longitude_arr`)<6000) AND (ABS(TIMEDIFF(?, `debut`)) < "20001") AND (`date`=? OR `mardi_rec`=1 OR DAYOFWEEK(`date`)=3) ORDER BY `distance` ASC, `diff` ASC;');
        $request=$sth->execute(array($latitude_dep,$longitude_dep,$debut,$user,$latitude_dep,$longitude_dep,$latitude_arr,$longitude_arr,$debut,$voyage->date));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetRecherche');
        $request_succeeded = $sth->execute();
        if ($request_succeeded){
            $trajets = $sth->fetchAll();
            $sth->closeCursor();
            $trajets_total=array_merge($trajets_total,$trajets);
            $request_total=$request_total+1;
        }
        
        }
        if (  $voyage->mercredi_pro==1 || $jour==3){
        $sth=$dbh->prepare('SELECT *,get_distance_metres(?,?,`latitude_dep`, `longitude_dep`) AS `distance`,ABS(TIMEDIFF(?, `debut`)) AS `diff` FROM `trajetrecherche` WHERE (`demandeur`<>?) AND (get_distance_metres(?,?,`latitude_dep`, `longitude_dep`)<2000) AND (get_distance_metres(?,?,`latitude_arr`, `longitude_arr`)<6000) AND (ABS(TIMEDIFF(?, `debut`)) < "20001") AND (`date`=? OR `mercredi_rec`=1 OR DAYOFWEEK(`date`)=4) ORDER BY `distance` ASC, `diff` ASC;');
        $request=$sth->execute(array($latitude_dep,$longitude_dep,$debut,$user,$latitude_dep,$longitude_dep,$latitude_arr,$longitude_arr,$debut,$voyage->date));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetRecherche');
        $request_succeeded = $sth->execute();
        if ($request_succeeded){
            $trajets = $sth->fetchAll();
            $sth->closeCursor();
            $trajets_total=array_merge($trajets_total,$trajets);
            $request_total=$request_total+1;
        }
        
        }
        if ( $voyage->jeudi_pro==1 || $jour==4){
        $sth=$dbh->prepare('SELECT *,get_distance_metres(?,?,`latitude_dep`, `longitude_dep`) AS `distance`,ABS(TIMEDIFF(?, `debut`)) AS `diff` FROM `trajetrecherche` WHERE (`demandeur`<>?) AND (get_distance_metres(?,?,`latitude_dep`, `longitude_dep`)<2000) AND (get_distance_metres(?,?,`latitude_arr`, `longitude_arr`)<6000) AND (ABS(TIMEDIFF(?, `debut`)) < "20001") AND (`date`=? OR `jeudi_rec`=1 OR DAYOFWEEK(`date`)=5) ORDER BY `distance` ASC, `diff` ASC;');
        $request=$sth->execute(array($latitude_dep,$longitude_dep,$debut,$user,$latitude_dep,$longitude_dep,$latitude_arr,$longitude_arr,$debut,$voyage->date));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetRecherche');
        $request_succeeded = $sth->execute();
        if ($request_succeeded){
            $trajets = $sth->fetchAll();
            $sth->closeCursor();
            $trajets_total=array_merge($trajets_total,$trajets);
            $request_total=$request_total+1;
        }
        
        }
        if ( $voyage->vendredi_pro==1 || $jour==5){
        $sth=$dbh->prepare('SELECT *,get_distance_metres(?,?,`latitude_dep`, `longitude_dep`) AS `distance`,ABS(TIMEDIFF(?, `debut`)) AS `diff` FROM `trajetrecherche` WHERE (`demandeur`<>?) AND (get_distance_metres(?,?,`latitude_dep`, `longitude_dep`)<2000) AND (get_distance_metres(?,?,`latitude_arr`, `longitude_arr`)<6000) AND (ABS(TIMEDIFF(?, `debut`)) < "20001") AND (`date`=? OR `vendredi_rec`=1 OR DAYOFWEEK(`date`)=6) ORDER BY `distance` ASC, `diff` ASC;');
        $request=$sth->execute(array($latitude_dep,$longitude_dep,$debut,$user,$latitude_dep,$longitude_dep,$latitude_arr,$longitude_arr,$debut,$voyage->date));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetRecherche');
        $request_succeeded = $sth->execute();
        if ($request_succeeded){
            $trajets = $sth->fetchAll();
            $sth->closeCursor();
            $trajets_total=array_merge($trajets_total,$trajets);
            $request_total=$request_total+1;
        }
        
        }
        if ( $voyage->samedi_pro==1 || $jour==6){
        $sth=$dbh->prepare('SELECT *,get_distance_metres(?,?,`latitude_dep`, `longitude_dep`) AS `distance`,ABS(TIMEDIFF(?, `debut`)) AS `diff` FROM `trajetrecherche` WHERE (`demandeur`<>?) AND (get_distance_metres(?,?,`latitude_dep`, `longitude_dep`)<2000) AND (get_distance_metres(?,?,`latitude_arr`, `longitude_arr`)<6000) AND (ABS(TIMEDIFF(?, `debut`)) < "20001") AND (`date`=? OR `samedi_rec`=1 OR DAYOFWEEK(`date`)=7) ORDER BY `distance` ASC, `diff` ASC;');
        $request=$sth->execute(array($latitude_dep,$longitude_dep,$debut,$user,$latitude_dep,$longitude_dep,$latitude_arr,$longitude_arr,$debut,$voyage->date));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetRecherche');
        $request_succeeded = $sth->execute();
        if ($request_succeeded){
            $trajets = $sth->fetchAll();
            $sth->closeCursor();
            $trajets_total=array_merge($trajets_total,$trajets);
            $request_total=$request_total+1;
        }
        
        }
        
        if ($request_total>0) {
            $trajets_en_plus= array_unique(array_udiff($trajets_total, TrajetPropose::getAllMatching1($dbh,$id_voyage),'Trajetrecherche::memeTrajet'),SORT_REGULAR);
            if (count($trajets_en_plus)==0){
                return NULL;
            }else{
                return $trajets_en_plus;
            }
        } else {
            return NULL;
        }
    }
    
    static function memeTrajet($voyage1,$voyage2){
        $id_voyage1=$voyage1->voyagepro;
        $id_voyage2=$voyage2->voyagepro;
        if ($id_voyage1==$id_voyage2){
            return 0;
        }else{
            return 1;
        }
    }
    
    //LOCATION----------------------------------------------------------------------------------------------------------------------------------------------
    
    function getAllLocation($dbh){
        $sth = $dbh->prepare('SELECT `latitude_dep`,`longitude_dep`,`latitude_arr`,`longitude_arr` FROM `trajetpropose`');
        $sth->execute();
        $sth->setFetchMode(PDO::FETCH_CLASS, 'Location');
        $request_succeeded = $sth->execute();
        if ($request_succeeded) {
            $locations = $sth->fetchAll();
            $sth->closeCursor();
            return $locations;
        } else {
            return NULL;
        }
    }
    
    function getTrajetPro($dbh,$voyagepro){
        $sth = $dbh->prepare('SELECT * FROM `trajetpropose` WHERE `voyagepro`=?;');
        $sth->execute(array($voyagepro));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetPropose');
        $request_succeeded = $sth->execute();
        if ($request_succeeded) {
            $user = $sth->fetch();
            $sth->closeCursor();
            return $user;
        } else {
            return NULL;
        }
    }
    
    //AFFICHAGE-------------------------------------------------------------------------------------------------------------------------------------------------
    
    function afficher($dbh,$voyagepro) {
        $trajet= TrajetPropose::getTrajetPro($dbh,$voyagepro);
        $tous_les= TrajetPropose::tous_les_pro($trajet);
        if ($trajet==NULL){
            echo "Erreur, ce trajet n'est pas dans notre base de données ...";
        }
        else {
            echo <<<FIN
            <div class="card text-center" style=" background-color: #dbdad9">
                    <h5 class="card-header titre_carte_trajet">Proposition de trajet</h5>
                    <div class='card-body'>
                       
                        <dl class="row">
                            
                                <dt class="col-3">Départ : </dt>
                                <dd class="col-3">$trajet->depart</dd>
                                
                                <dt class="col-3">Arrivée : </dt>
                                <dd class="col-3">$trajet->arrivee</dd>

                                <dt class="col-3">Départ à: </dt>
                                <dd class="col-3">$trajet->debut</dd>

                                <dt class="col-3">Arrivée à : </dt>
                                <dd class="col-3">$trajet->fin</dd>
                    
                                <dt class="col">Quand ? </dt>
                                <dd class="col-9">$tous_les</dd>

                                <dt class="col-sm-3">Etapes possibles :</dt>
                                <dd class="col-sm-7">$trajet->etapes</dd>

                                <dt class="col-sm-3">Places :</dt>
                                <dd class="col-sm-7">$trajet->places</dd>
                         
                        </dl>
            FIN;
            $user=Utilisateur::getUtilisateur($dbh,$trajet->conducteur);
            Utilisateur::afficher($user);
            echo "</div>";
            echo "</div>";
            echo "<br>";
            
        }
    }

    
    function afficherPersonal($dbh,$voyagepro) {
        $trajet= TrajetPropose::getTrajetPro($dbh,$voyagepro);
        $tous_les= TrajetPropose::tous_les_pro($trajet);
        if ($trajet==NULL){
            echo "Erreur, ce trajet n'est pas dans notre base de données ...";
        }
        else {
            echo <<<FIN
            <div class="card text-center" style=" background-color: #dbdad9">
                    <h5 class="card-header titre_carte_trajet">Proposition de trajet</h5>
                    <div class='card-body'>
                       
                        <dl class="row">
                            
                                <dt class="col-3">Départ : </dt>
                                <dd class="col-3">$trajet->depart</dd>
                                
                                <dt class="col-3">Arrivée : </dt>
                                <dd class="col-3">$trajet->arrivee</dd>

                                <dt class="col-3">Départ à: </dt>
                                <dd class="col-3">$trajet->debut</dd>

                                <dt class="col-3">Arrivée à : </dt>
                                <dd class="col-3">$trajet->fin</dd>
                    
                                <dt class="col">Quand ? </dt>
                                <dd class="col-9">$tous_les</dd>

                                <dt class="col-sm-3">Etapes possibles :</dt>
                                <dd class="col-sm-7">$trajet->etapes</dd>

                                <dt class="col-sm-3">Places :</dt>
                                <dd class="col-sm-7">$trajet->places</dd>
                         
                        </dl>
            FIN;
            $user=Utilisateur::getUtilisateur($dbh,$trajet->conducteur);
            
            echo "</div>";
            echo "</div>";
            echo "<br>";
            
        }
    }    

    //MODIFICATION DATABASE---------------------------------------------------------------------------------------------------------------------------------------------
    
    function insererVoyagePro($dbh, $email, $depart,$latitude_dep,$longitude_dep, $arrivee,$latitude_arr,$longitude_arr, $debut, $fin, $date, $places, $lundi_pro,$mardi_pro,$mercredi_pro,$jeudi_pro,$vendredi_pro,$samedi_pro,$dimanche_pro, $etapes, $infosupp, $kilometrage) {
        $sth = $dbh->prepare("INSERT INTO `trajetpropose` (`conducteur`, `depart`,`latitude_dep`,`longitude_dep`, `arrivee`,`latitude_arr`,`longitude_arr`, `debut`, `fin`, `date`, `places`,`infosupp`,`lundi_pro`,`mardi_pro`,`mercredi_pro`,`jeudi_pro`,`vendredi_pro`,`samedi_pro`,`dimanche_pro`,`kilometrage`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $sth->execute(array($email, htmlspecialchars($depart),$latitude_dep,$longitude_dep, htmlspecialchars($arrivee),$latitude_arr,$longitude_arr, $debut, $fin, $date, $places, htmlspecialchars($infosupp), $lundi_pro,$mardi_pro,$mercredi_pro,$jeudi_pro,$vendredi_pro,$samedi_pro,$dimanche_pro, $kilometrage));
        if ($etapes != null) {
            $idvoyage = $dbh->lastInsertId();
            foreach ($etapes as $lieu) {
                if ($lieu!=""){
                $sth = $dbh->prepare("INSERT INTO `etapes` (`voyage`,`lieuetape`) VALUES (?,?)");
                $sth->execute(array($idvoyage, htmlspecialchars($lieu)));
                }
            }
        }
    }

    function getAllVoyagePro($dbh) {
        $sth = $dbh->prepare("SELECT * FROM `trajetpropose` WHERE `date`>?OR (lundi_pro!=0) OR (mardi_pro!=0) OR (mercredi_pro!=0) OR (jeudi_pro!=0) OR (vendredi_pro!=0) OR (samedi_pro!=0) OR (dimanche_pro!=0)");
        $now=date('Y-m-d');
        $sth->execute(array($now));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetPropose');
        $request_succeeded = $sth->execute();
        if ($request_succeeded) {
            $trajets = $sth->fetchAll();
            $sth->closeCursor();
            return $trajets;
        } else {
            return NULL;
        }
    }
    
    function getAllEtapes($dbh,$trajet){
        $sth = $dbh->prepare("SELECT lieuetape FROM `etapes` WHERE `voyage`=?");
        $sth->execute(array($trajet->voyagepro));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'Etape');
        $request_succeeded = $sth->execute();
        if ($request_succeeded) {
            $etapes = $sth->fetchAll();
            $sth->closeCursor();
            return $etapes;
        } else {
            return NULL;
        }
    }
    
     function getTrajetsProUser($dbh,$email) {
        $sth = $dbh->prepare("SELECT `voyagepro` FROM `trajetpropose` WHERE conducteur=?");
        $sth->execute(array($email));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'TrajetRecherche');
        $request_succeeded = $sth->execute();
        if ($request_succeeded) {
            $trajets = $sth->fetchAll();
            $sth->closeCursor();
            return $trajets;
        } else {
            return NULL;
        }
    }
    
    function tous_les_pro($trajet){
    if ($trajet->lundi_pro+$trajet->mardi_pro+$trajet->mercredi_pro+$trajet->jeudi_pro+$trajet->vendredi_pro+$trajet->samedi_pro+$trajet->dimanche_pro==0){
        return($trajet->date);
    }
    else{
        $retour="Tous les ";
        if ($trajet->lundi_pro!=0){
            
            $retour=$retour."lundis, ";
        }
        if ($trajet->mardi_pro!=0){
            $retour=$retour."mardis, ";
        }
        if ($trajet->mercredi_pro!=0){
            $retour=$retour."mercredis, ";
        }
        if ($trajet->jeudi_pro==1){
            $retour=$retour."jeudis, ";
        }
        if ($trajet->vendredi_pro==1){
            $retour=$retour."vendredis, ";
        }
        if ($trajet->samedi_pro==1){
            $retour=$retour."samedis, ";
        }
        if ($trajet->dimanche_pro==1){
            $retour=$retour."dimanches, ";
        }
        $retour=substr($retour,0, -2);
        return ($retour);
    }
}

    public static function delete($dbh, $id) {
        $sth = $dbh->prepare("DELETE FROM `trajetpropose` WHERE `voyagepro`=?;");
        $sth->execute(array($id));
    }

}
