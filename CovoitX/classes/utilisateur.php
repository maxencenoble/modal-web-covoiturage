<?php

class Utilisateur {

    public $email;
    public $nom;
    public $prenom;
    public $mdp;
    public $naissance;
    public $contact;
    public $descriptif;
    public $photo;
    public $admin;

    //SCORE---------------------------------------------------------------------------
        function getScoreEcoDemandeur($dbh,$user){
        $login=$user->email;
        $sth1 = $dbh->prepare('SELECT SUM(get_distance_metres(`latitude_dep`, `longitude_dep`,`latitude_arr`, `longitude_arr`)) AS `somme` FROM `trajetrecherche` WHERE `demandeur`=?;');
        $total=$sth1->execute(array($login));
        $sth2 = $dbh->prepare('SELECT SUM(get_distance_metres(`latitude_dep`, `longitude_dep`,`latitude_arr`, `longitude_arr`)) AS `somme` FROM `trajetrecherche` JOIN `trajetaccepte` ON `demandeur`=`client` AND `voyagerec`=`idvoyage` WHERE `demandeur`=? AND `statut`="recherche";');
        $request=$sth2->execute(array($login));
        $sth3 = $dbh->prepare('SELECT * FROM `trajetrecherche` WHERE `demandeur`=?;');
        $request3=$sth3->execute(array($login));
        $reponse1=$sth1->fetch();
        $reponse2=$sth2->fetch();
        $reponse3=$sth3->rowCount();
        if($reponse1['somme']>0){
         return max(round(100*$reponse2['somme']/$reponse1['somme']),min($reponse3,50));   
        }else{
            return min($reponse3,50);
        }
        
    }
    
    function getScoreEcoConducteur($dbh,$user){
        $login=$user->email;
        $sth1 = $dbh->prepare('SELECT SUM(get_distance_metres(`latitude_dep`, `longitude_dep`,`latitude_arr`, `longitude_arr`)*`places`) AS `somme`FROM `trajetpropose` WHERE `conducteur`=?;');
        $total=$sth1->execute(array($login));
        $sth2 = $dbh->prepare('SELECT SUM(get_distance_metres(`latitude_dep`, `longitude_dep`,`latitude_arr`, `longitude_arr`)* `places`) AS `somme` FROM `trajetpropose` JOIN `trajetaccepte` ON `conducteur`=`client` AND `voyagepro`=`idvoyage` WHERE `conducteur`=? AND `statut`="propose";');
        $request=$sth2->execute(array($login));
        $sth3 = $dbh->prepare('SELECT * FROM `trajetpropose`  WHERE `conducteur`=? ;');
        $request3=$sth3->execute(array($login));
        $reponse1=$sth1->fetch();
        $reponse2=$sth2->fetch();
        $reponse3=$sth3->rowCount();
        if($reponse1['somme']>0){
         return max(round(100*$reponse2['somme']/$reponse1['somme']),min($reponse3,50));   
        }else{
            return min($reponse3,50);
        }
    }
    
    
    //AFFICHAGE-----------------------------------------------------------------------------------------
    function afficher($user) {
        echo <<<FIN
        <div class="card text-center" style=" background-color: #f6ae4e">
                <div class='card-body'>
                    <h5 class="card-title"> $user->prenom $user->nom  (LOGIN : $user->email)</h5>
                    <div class="row">
                        <div class="col">
        FIN;
        Utilisateur::afficher_photo($user,100);
        echo <<<FIN
                        </div>
                        <div class="col">
                        <ul class="list-group" style=" max-width: 30rem">
                            
                            <li class="list-group-item">$user->descriptif</li>
                            <li class="list-group-item">$user->contact</li>
                        </ul>
                        </div>
                    </div>
                </div>
        </div>
        FIN;
    }
    
    function afficher_photo($user, $width){
        
        if ($user->photo==NULL){
            echo "<img  src='https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/Antu_system-switch-user.svg/768px-Antu_system-switch-user.svg.png' alt='userpic' style='width:".$width."px'>";
        }
        else {
            $cle= sha1($user->email);
            echo "<img  src='images/photo_LD_".$cle.".jpg' alt='userpic' style='width:".$width."px'>";
            
        }
    }

    //MODIFICATIONS UTILISATEUR---------------------------------------------------------------------------------------------------
    public static function getUtilisateur($dbh, $email) {
        $sth = $dbh->prepare('SELECT * FROM `utilisateurs` WHERE `email`=?;');
        $sth->execute(array($email));
        $sth->setFetchMode(PDO::FETCH_CLASS, 'Utilisateur');
        $request_succeeded = $sth->execute();
        if ($request_succeeded) {
            $user = $sth->fetch();
            $sth->closeCursor();
            return $user;
        } else {
            return NULL;
        }
    }

    function insererUtilisateur($dbh, $email, $nom, $prenom, $mdp, $naissance, $contact, $descriptif, $photo) {
        if (utilisateur::getUtilisateur($dbh, $email) == NULL) {
            $sth = $dbh->prepare("INSERT INTO `utilisateurs` (`email`, `nom`, `prenom`, `mdp`, `naissance`, `contact`, `descriptif`,`photo`) VALUES(?,?,?,SHA1(?),?,?,?,?)");
            $sth->execute(array($email, htmlspecialchars($nom), htmlspecialchars($prenom), $mdp."@&**4+5", $naissance, htmlspecialchars($contact), htmlspecialchars($descriptif), $photo));
        } else {
            echo "<h1> ntm fdp <\h1>";
        }
    }

    function modifierMdp($dbh, $user, $newmdp) {
        $sth = $dbh->prepare("UPDATE `utilisateurs` SET `mdp`=SHA1(?) WHERE `email`=?;");
        $sth->execute(array($newmdp."@&**4+5", $user->email));
    }
    
    function modifier($dbh,$user,$nom,$prenom,$contact,$naissance,$descriptif,$photo){
        $sth = $dbh->prepare("UPDATE `utilisateurs` SET `nom`=?, `prenom`=?, `contact`=?, `naissance`=?,`descriptif`=?, `photo`=? WHERE `email`=?;");
        $sth->execute(array(htmlspecialchars($nom),htmlspecialchars($prenom),htmlspecialchars($contact),$naissance,htmlspecialchars($descriptif),$photo, $user->email));
       
    }

    public static function testerMdp($dbh, $user, $mdp) {
        if ($user == null) {
            return FALSE;
        } else {
            if (sha1($mdp."@&**4+5") == $user->mdp) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    //pas modifié, pas utile pour l'instant (éventuellement adapter avec getTrajets)
    public static function getAmis($dbh, $login) {
        $query = 'SELECT a.login, a.mdp, a.nom, a.prenom, a.promotion, a.naissance, a.email, a.feuille FROM utilisateurs as a, utilisateurs as b, amis WHERE (login1=a.login)AND(login2=b.login)AND(login2="' . $login . '");';
        $sth = $dbh->prepare($query);
        $sth->setFetchMode(PDO::FETCH_CLASS, 'Utilisateur');
        $request_succeeded = $sth->execute();
        if ($request_succeeded) {
            $amis = $sth->fetchAll();
            $sth->closeCursor();
            return $amis;
        } else {
            return NULL;
        }
    }
    
    public static function getAll($dbh) {
        $query = 'SELECT * FROM utilisateurs WHERE (`admin`!=1);';
        $sth = $dbh->prepare($query);
        $sth->setFetchMode(PDO::FETCH_CLASS, 'Utilisateur');
        $request_succeeded = $sth->execute();
        if ($request_succeeded) {
            $tlm = $sth->fetchAll();
            $sth->closeCursor();
            return $tlm;
        } else {
            return NULL;
        }
    }

    //pas modifié, pas utile pour l'instant (éventuellement adapter avec pageTrajets
    public static function pageAmis($dbh, $login) {
        $tab = Utilisateur::getAmis($dbh, $login);
        generateHTMLHeader("Amis de " . $login, "mafeuille.css");
        echo "<h1> Amis de " . $login . "</h1>";
        foreach ($tab as $ami) {
            echo "<br>";
            echo $ami;
        }
    }

    public static function deleteUser($dbh, $email) {
        $sth = $dbh->prepare("DELETE FROM `utilisateurs` WHERE `email`=?;");
        $sth->execute(array($email));
    }

}
?>



