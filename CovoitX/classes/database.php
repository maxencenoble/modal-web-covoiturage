<?php

class Database {

    public static function connect() {
        $dsn = 'mysql:dbname=covoiturage;host=127.0.0.1';
        $user = 'root';
        $password = '';
        $dbh = null;
        try {
            $dbh = new PDO($dsn, $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Connexion échouée : ' . $e->getMessage();
            exit(0);
        }
        return $dbh;
    }

    function insererUtilisateur($dbh, $login, $mdp, $nom, $prenom, $promotion, $naissance, $email, $feuille) {
        $sth = $dbh->prepare("INSERT INTO `utilisateurs` (`login`, `mdp`, `nom`, `prenom`, `promotion`, `naissance`, `email`, `feuille`) VALUES(?,SHA1(?),?,?,?,?,?,?)");
        $sth->execute(array($login, $mdp, $prenom, $nom, $promotion, $naissance, $email, $feuille));
    }

    public static function executerRequete($dbh, $query) {
        $sth = $dbh->prepare($query);
        $request_succeeded = $sth->execute();
        if ($request_succeeded) {
            while ($courant = $sth->fetch(PDO::FETCH_ASSOC)){
                echo "nom : ".$courant['nom']." prenom : ".$courant['prenom'];
                echo "<br>";
            }
        }
    }

}


?>


