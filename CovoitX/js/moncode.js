

var geocoder;

function initMap() {
    geocoder = new google.maps.Geocoder();
}

$(document).ready(function () {
    
    $("#ajoutTag").click(function () {
        event.preventDefault();
        $("#etapes").append('<input id="10" class="form-control" type=text name="etapes[]"/><br/>');
    });
    
    $('#voirtrajets1').DataTable();
    $('#voirtrajets2').DataTable();
    $('#admintrajets1').DataTable();
    $('#admintrajets2').DataTable();
    $('#adminusers').DataTable();
    $('.zoomadmin').hide();
    
    
    $('.matching').hide();
    $('#bouton_dep').hide();
    $('#bouton_arr').hide();
    $('#carte_dep').hide();
    $('#carte_arr').hide();  
    $('#carte_voir_pro').hide();
    $('#carte_voir_rec').hide();
    
    $('.matching2_non_null').hide();
    $('.matching2_null').hide();
    
    $('#btn-matching2_non_null').click(function () {
        $( ".matching2_non_null" ).fadeIn( "slow");
        document.getElementsByClassName('matching2_non_null').style.display='block';
    });
    $('#btn-matching2_null').click(function () {
        $( ".matching2_null" ).fadeIn( "slow");
        document.getElementsByClassName('matching2_null').style.display='block';
    });
    
    
    var map;
    
    var map_tout;
    
    function initializeVoirTout(){
        var latitude = 48.714453;
        var longitude = 2.211324;
        var mapOptionsTout = {
            zoom: 15,
            center: new google.maps.LatLng(latitude, longitude)
        };
        map_tout = new google.maps.Map(document.getElementById('map-canvas_voir_tout'), mapOptionsTout);
    }
    
  
    function initializeVoirDep(lat, lon,prefixe) {
        var mapOptions = {
            zoom: 15,
            center: new google.maps.LatLng(lat, lon)
        };
        if (prefixe=='pro'){
        map = new google.maps.Map(document.getElementById('map-canvas_voir_pro_dep'), mapOptions);}
    else{
        map = new google.maps.Map(document.getElementById('map-canvas_voir_rec_dep'), mapOptions);
    }
    }
    
    function initializeVoirArr(lat, lon,prefixe) {
        var mapOptions = {
            zoom: 15,
            center: new google.maps.LatLng(lat, lon)
        };
        if (prefixe=='pro'){
        map = new google.maps.Map(document.getElementById('map-canvas_voir_pro_arr'), mapOptions);}
        else{
        map = new google.maps.Map(document.getElementById('map-canvas_voir_rec_arr'), mapOptions);
        }
    }
    
    $(".btn-ensavoirplus_pro").click(function () {
        event.preventDefault();
        var coordonnees = $(this).attr("id");
        var tab = coordonnees.split(';');
        var lat_dep = tab[0];
        var long_dep = tab[1];
        var lat_arr = tab[2];
        var long_arr = tab[3];
        var places_pro = tab[4];
        var info_supp_pro = tab[5];
        var prenom=tab[6];
        var nom=tab[7];
        var contact=tab[8];
        var id_pro=tab[9];
        initializeVoirDep(lat_dep, long_dep,'pro');
        var myLatlng = new google.maps.LatLng(lat_dep, long_dep);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Lieu de départ'
        });
        initializeVoirArr(lat_arr, long_arr,'pro');
        var myLatlng = new google.maps.LatLng(lat_arr, long_arr);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Lieu darrivée'
        });
        $('#conducteur').empty();
        $('#conducteur').append(prenom + " " + nom);
        $('#contact_pro').empty();
        $('#contact_pro').append(contact);
        $('#places_pro').empty();
        $('#places_pro').append(places_pro);
        $('#info_supp_pro').empty();
        $('#info_supp_pro').append(info_supp_pro);
        $('#bouton_pro').empty();
        $('#bouton_pro').append("<a href='index.php?page=zoomtrajet&type=pro&id=" +id_pro+" class='btn btn-primary btn-lg active' role='button' aria-pressed='true'>Plus d'infos ?</a>");
        $( "#carte_voir_pro" ).fadeIn( "slow");
        document.getElementById('carte_voir_pro').style.display='block';
    });


    $(".btn-ensavoirplus_rec").click(function () {
        event.preventDefault();
        var coordonnees = $(this).attr("id");
        var tab = coordonnees.split(';');
        var lat_dep = tab[0];
        var long_dep = tab[1];
        var lat_arr = tab[2];
        var long_arr = tab[3];
        var categorie =tab[4];
        var info_supp_rec = tab[5];
        var prenom=tab[6];
        var nom=tab[7];
        var contact=tab[8];
        var id_rec=tab[9];
        initializeVoirDep(lat_dep, long_dep,'rec');
        var myLatlng = new google.maps.LatLng(lat_dep, long_dep);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Lieu de départ'
        });
        initializeVoirArr(lat_arr, long_arr,'rec');
        var myLatlng = new google.maps.LatLng(lat_arr, long_arr);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Lieu darrivée'
        });
        $('#demandeur').empty();
        $('#demandeur').append(prenom + " " + nom);
        $('#contact_rec').empty();
        $('#contact_rec').append(contact);
        $('#categorie').empty();
        $('#categorie').append(categorie);
        $('#info_supp_rec').empty();
        $('#info_supp_rec').append(info_supp_rec);
        $('#bouton_rec').empty();
        $('#bouton_rec').append("<a href='index.php?page=zoomtrajet&type=rec&id=" +id_rec+" class='btn btn-primary btn-lg active' role='button' aria-pressed='true'>Plus d'infos ?</a>");
        $( "#carte_voir_rec" ).fadeIn( "slow");
        document.getElementById('carte_voir_rec').style.display='block';
    });
    
    $(".btn-admin_pro").click(function () {
        event.preventDefault();
        var id = $(this).attr("id");
        $('#zoomadminpro'+id).show();
        document.getElementById('#zoomadminpro'+id).style.display='block';
    });
    
    $(".btn-admin_rec").click(function () {
        event.preventDefault();
        var id = $(this).attr("id");
        $('#zoomadminrec'+id).show();
        document.getElementById('#zoomadminrec'+id).style.display='block';
    });
    
    $(".btn-admin_user").click(function () {
        event.preventDefault();
        var id = $(this).attr("id");
        $('#zoomuser'+SHA1(id)).show();
    });
   
    
    
    
    
    //en ce qui concerne les cartes
    var map;

    function initializeDep(lat, lon) {
        var mapOptions = {
            zoom: 15,
            center: new google.maps.LatLng(lat, lon)
        };
        map = new google.maps.Map(document.getElementById('map-canvas_dep'), mapOptions);
    }

    // bouton affichage de la carte au depart
    $("#btn-carte_dep").click(function () {
        event.preventDefault();//pour ne pas soumettre le formulaire associé
        // affichage de la carte
        var address = $("#depart").val();
        if (address === "") {//adresse par défaut
            address = "Ecole Polytechnique, Palaiseau";
            $("#depart").val(address);
        }
        // Si les coordonnées ne sont pas définies on centre sur Polytechnique
        var latitude = $("#latitude_dep").val();
        if (latitude == "") {
            latitude = 48.714453;
        }
        var longitude = $("#longitude_dep").val();
        if (longitude == "") {
            longitude = 2.211324;
        }
        initializeDep(latitude, longitude);
        document.getElementById('carte_dep').style.display='block';
        // ajout du marqueur
        var myLatlng = new google.maps.LatLng(latitude, longitude);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Adresse demandée'
        });

        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(event.latLng);
        });

        
        function placeMarker(location) {
            if (marker) { //on vérifie si le marqueur existe
                marker.setPosition(location); //on change sa position
            } else {
                marker = new google.maps.Marker({//on créé le marqueur
                    position: location,
                    map: map,
                    title : 'Adresse demandée'
                });
            }
            $("#latitude_dep").val(location.lat());
            $("#longitude_dep").val(location.lng());
        }


        
        });
    

    function initializeArr(lat, lon) {
        var mapOptions = {
            zoom: 15,
            center: new google.maps.LatLng(lat, lon)
        };
        map = new google.maps.Map(document.getElementById('map-canvas_arr'), mapOptions);
    }

    //bouton affichage de la carte au depart
    $("#btn-carte_arr").click(function () {
        event.preventDefault();//pour ne pas soumettre le formulaire associé
        // affichage de la carte
        var address = $("#arrivee").val();
        if (address === "") {//adresse par défaut
            address = "Ecole Polytechnique, Palaiseau";
            $("#arrivee").val(address);
        }
        // Si les coordonnées ne sont pas définies on centre sur Polytechnique
        var latitude = $("#latitude_arr").val();
        // Si les coordonnées ne sont pas définies on centre sur Londres
        if (latitude == "") {
            latitude = 48.714453;
        }
        var longitude = $("#longitude_arr").val();
        if (longitude == "") {
            longitude = 2.211324;
        }
        initializeArr(latitude, longitude);
        document.getElementById('carte_arr').style.display='block';
        // ajout du marqueur
        var myLatlng = new google.maps.LatLng(latitude, longitude);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Adresse demandée'
        });
        
        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(event.latLng);
        });

        
        function placeMarker(location) {
            if (marker) { //on vérifie si le marqueur existe
                marker.setPosition(location); //on change sa position
            } else {
                marker = new google.maps.Marker({//on créé le marqueur
                    position: location,
                    map: map,
                    title: 'Adresse demandée'
                });
            }
            $("#latitude_arr").val(location.lat());
            $("#longitude_arr").val(location.lng());
        }
        
        
    });



    function codeAddressDep(address) {
        geocoder.geocode({'address': address}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var lat = results[0].geometry.location.lat();
                var long = results[0].geometry.location.lng();
                $("#latitude_dep").val(lat);
                $("#longitude_dep").val(long);


            } else {
                alert("Problème survenu lors de la saisie de l'adresse");
            }
        });
    }

    function codeAddressArr(address) {
        geocoder.geocode({'address': address}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var lat = results[0].geometry.location.lat();
                var long = results[0].geometry.location.lng();
                $("#latitude_arr").val(lat);
                $("#longitude_arr").val(long);


            } else {
                alert("Problème survenu lors de la saisie de l'adresse");
            }
        });
    }

    $("#btn-geodepart").click(function () {
        event.preventDefault();//pour ne pas soumettre le formulaire associé
        var address = $("#depart").val();
        if (address === "") {//adresse par défaut
            address = "Ecole Polytechnique, Palaiseau";
            $("#depart").val(address);
        }
        codeAddressDep(address);
        $( "#bouton_pro_dep" ).fadeIn( "slow");
        document.getElementById('bouton_dep').style.display='block';
    });

    $("#btn-geoarrivee").click(function () {
        event.preventDefault();//pour ne pas soumettre le formulaire associé
        var address = $("#arrivee").val();
        if (address === "") {//adresse par défaut
            address = "Ecole Polytechnique, Palaiseau";
            $("#arrivee").val(address);

        }
        codeAddressArr(address);
        $( "#bouton_pro_arr" ).fadeIn( "slow");
        document.getElementById('bouton_arr').style.display='block';
    });
    
    
    initializeVoirTout();
});

//Fonction SHA1

/**
* Secure Hash Algorithm (SHA1)
* http://www.webtoolkit.info/
**/
function SHA1(msg) {
 function rotate_left(n,s) {
 var t4 = ( n<<s ) | (n>>>(32-s));
 return t4;
 };
 function lsb_hex(val) {
 var str='';
 var i;
 var vh;
 var vl;
 for( i=0; i<=6; i+=2 ) {
 vh = (val>>>(i*4+4))&0x0f;
 vl = (val>>>(i*4))&0x0f;
 str += vh.toString(16) + vl.toString(16);
 }
 return str;
 };
 function cvt_hex(val) {
 var str='';
 var i;
 var v;
 for( i=7; i>=0; i-- ) {
 v = (val>>>(i*4))&0x0f;
 str += v.toString(16);
 }
 return str;
 };
 function Utf8Encode(string) {
 string = string.replace(/\r\n/g,'\n');
 var utftext = '';
 for (var n = 0; n < string.length; n++) {
 var c = string.charCodeAt(n);
 if (c < 128) {
 utftext += String.fromCharCode(c);
 }
 else if((c > 127) && (c < 2048)) {
 utftext += String.fromCharCode((c >> 6) | 192);
 utftext += String.fromCharCode((c & 63) | 128);
 }
 else {
 utftext += String.fromCharCode((c >> 12) | 224);
 utftext += String.fromCharCode(((c >> 6) & 63) | 128);
 utftext += String.fromCharCode((c & 63) | 128);
 }
 }
 return utftext;
 };
 var blockstart;
 var i, j;
 var W = new Array(80);
 var H0 = 0x67452301;
 var H1 = 0xEFCDAB89;
 var H2 = 0x98BADCFE;
 var H3 = 0x10325476;
 var H4 = 0xC3D2E1F0;
 var A, B, C, D, E;
 var temp;
 msg = Utf8Encode(msg);
 var msg_len = msg.length;
 var word_array = new Array();
 for( i=0; i<msg_len-3; i+=4 ) {
 j = msg.charCodeAt(i)<<24 | msg.charCodeAt(i+1)<<16 |
 msg.charCodeAt(i+2)<<8 | msg.charCodeAt(i+3);
 word_array.push( j );
 }
 switch( msg_len % 4 ) {
 case 0:
 i = 0x080000000;
 break;
 case 1:
 i = msg.charCodeAt(msg_len-1)<<24 | 0x0800000;
 break;
 case 2:
 i = msg.charCodeAt(msg_len-2)<<24 | msg.charCodeAt(msg_len-1)<<16 | 0x08000;
 break;
 case 3:
 i = msg.charCodeAt(msg_len-3)<<24 | msg.charCodeAt(msg_len-2)<<16 | msg.charCodeAt(msg_len-1)<<8 | 0x80;
 break;
 }
 word_array.push( i );
 while( (word_array.length % 16) != 14 ) word_array.push( 0 );
 word_array.push( msg_len>>>29 );
 word_array.push( (msg_len<<3)&0x0ffffffff );
 for ( blockstart=0; blockstart<word_array.length; blockstart+=16 ) {
 for( i=0; i<16; i++ ) W[i] = word_array[blockstart+i];
 for( i=16; i<=79; i++ ) W[i] = rotate_left(W[i-3] ^ W[i-8] ^ W[i-14] ^ W[i-16], 1);
 A = H0;
 B = H1;
 C = H2;
 D = H3;
 E = H4;
 for( i= 0; i<=19; i++ ) {
 temp = (rotate_left(A,5) + ((B&C) | (~B&D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
 E = D;
 D = C;
 C = rotate_left(B,30);
 B = A;
 A = temp;
 }
 for( i=20; i<=39; i++ ) {
 temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
 E = D;
 D = C;
 C = rotate_left(B,30);
 B = A;
 A = temp;
 }
 for( i=40; i<=59; i++ ) {
 temp = (rotate_left(A,5) + ((B&C) | (B&D) | (C&D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
 E = D;
 D = C;
 C = rotate_left(B,30);
 B = A;
 A = temp;
 }
 for( i=60; i<=79; i++ ) {
 temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
 E = D;
 D = C;
 C = rotate_left(B,30);
 B = A;
 A = temp;
 }
 H0 = (H0 + A) & 0x0ffffffff;
 H1 = (H1 + B) & 0x0ffffffff;
 H2 = (H2 + C) & 0x0ffffffff;
 H3 = (H3 + D) & 0x0ffffffff;
 H4 = (H4 + E) & 0x0ffffffff;
 }
 var temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);

 return temp.toLowerCase();
}



