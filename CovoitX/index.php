<?php
session_name("NomDeSessionAModifierSelonVotreGout");
// ne pas mettre d'espace dans le nom de session !
session_start();
if (!isset($_SESSION['initiated'])) {
    session_regenerate_id();
    $_SESSION['initiated'] = true;
}

//A decommenter et remplir pour la fonctionnalité mail de zoneadmin
//ini_set('STMP', var );
// Décommenter la ligne suivante pour afficher le tableau $_SESSION pour le debuggage
// var_dump($_SESSION);


require('utilities/utils.php');
require('utilities/printForms.php');
require('utilities/logInOut.php');
require('classes/database.php');
require('classes/utilisateur.php');
require('classes/trajetpropose.php');
require('classes/trajetrecherche.php');
require('classes/location.php');


$dbh = Database::connect();

if ((isset($_GET['todo'])) && ($_GET["todo"] == "login")) {
    logIn($dbh);
}
if ((isset($_GET['todo'])) && ($_GET["todo"] == "logout")) {
    logOut();
}

if (isset($_GET['page'])) {
    $askedPage = $_GET['page'];
} else {
    $askedPage = "accueil";
}
$authorized = checkPage($askedPage, $page_list);
if ($authorized) {
    $pageTitle = getPageTitle($askedPage, $page_list);
} else {
    $pageTitle = "Erreur";
}

generateHTMLHeader($pageTitle, 'css/perso.css');
?>




<?php
generateMenu($page_list);
?>
<div class="container-fluid">
<div class="row no-padding">
    <div class="col-sm-2">
    
        <?php

        if (isset($_SESSION["loggedIn"])) {
            require("content/content_loggedInUser.php");
            printLogOutForm($askedPage);
        } else {
            printLoginForm($askedPage);
        }
        ?> 
    
    
      
    </div>

<div class="col-sm-8">   

        <?php
        if ($authorized) {
            require("content/content_" . $askedPage . ".php");
        } else {
            echo "<p>Désolé, la page demandée n'existe pas ou n'est accessible qu'aux gentlemen.</p>";
        }
        ?>

</div>
<div class="col-sm-2">    
        <?php
        require("content/content_score.php");
        ?>
    </div>
</div> 
</div>
<?php
generateHTMLFooter();
?>




