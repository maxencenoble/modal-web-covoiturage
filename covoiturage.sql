-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 25 fév. 2020 à 00:05
-- Version du serveur :  10.4.8-MariaDB
-- Version de PHP :  7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `covoiturage`
--

DELIMITER $$
--
-- Fonctions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `get_distance_metres` (`lat1` DOUBLE, `lng1` DOUBLE, `lat2` DOUBLE, `lng2` DOUBLE) RETURNS DOUBLE BEGIN
    DECLARE rlo1 DOUBLE;
    DECLARE rla1 DOUBLE;
    DECLARE rlo2 DOUBLE;
    DECLARE rla2 DOUBLE;
    DECLARE dlo DOUBLE;
    DECLARE dla DOUBLE;
    DECLARE a DOUBLE;
   
    SET rlo1 = RADIANS(lng1);
    SET rla1 = RADIANS(lat1);
    SET rlo2 = RADIANS(lng2);
    SET rla2 = RADIANS(lat2);
    SET dlo = (rlo2 - rlo1) / 2;
    SET dla = (rla2 - rla1) / 2;
    SET a = SIN(dla) * SIN(dla) + COS(rla1) * COS(rla2) * SIN(dlo) * SIN(dlo);
    RETURN (6378137 * 2 * ATAN2(SQRT(a), SQRT(1 - a)));
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `etapes`
--

CREATE TABLE `etapes` (
  `id` int(11) NOT NULL,
  `voyage` int(11) NOT NULL,
  `lieuetape` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `etapes`
--

INSERT INTO `etapes` (`id`, `voyage`, `lieuetape`) VALUES
(4, 9, 'Devant Télécom'),
(5, 10, 'Centrale'),
(6, 10, 'Télécom'),
(7, 10, 'Paris XIV'),
(8, 12, 'Paris XIII'),
(9, 12, 'Versailles'),
(10, 14, 'Angers'),
(11, 14, 'Cherbourg');

-- --------------------------------------------------------

--
-- Structure de la table `trajetaccepte`
--

CREATE TABLE `trajetaccepte` (
  `id` int(11) NOT NULL,
  `idvoyage` int(11) NOT NULL,
  `client` varchar(128) NOT NULL,
  `statut` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `trajetpropose`
--

CREATE TABLE `trajetpropose` (
  `voyagepro` int(11) NOT NULL,
  `conducteur` varchar(128) NOT NULL,
  `depart` varchar(128) NOT NULL,
  `latitude_dep` varchar(128) NOT NULL,
  `longitude_dep` varchar(128) NOT NULL,
  `arrivee` varchar(128) NOT NULL,
  `latitude_arr` varchar(128) NOT NULL,
  `longitude_arr` varchar(128) NOT NULL,
  `debut` time NOT NULL,
  `fin` time NOT NULL,
  `date` date NOT NULL,
  `places` int(11) NOT NULL,
  `infosupp` text DEFAULT NULL,
  `lundi_pro` int(1) NOT NULL,
  `mardi_pro` int(1) NOT NULL,
  `mercredi_pro` int(1) NOT NULL,
  `jeudi_pro` int(1) NOT NULL,
  `vendredi_pro` int(1) NOT NULL,
  `samedi_pro` int(1) NOT NULL,
  `dimanche_pro` int(1) NOT NULL,
  `kilometrage` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `trajetpropose`
--

INSERT INTO `trajetpropose` (`voyagepro`, `conducteur`, `depart`, `latitude_dep`, `longitude_dep`, `arrivee`, `latitude_arr`, `longitude_arr`, `debut`, `fin`, `date`, `places`, `infosupp`, `lundi_pro`, `mardi_pro`, `mercredi_pro`, `jeudi_pro`, `vendredi_pro`, `samedi_pro`, `dimanche_pro`, `kilometrage`) VALUES
(9, 'maxence.noble@gmail.com', 'Ecole Polytechnique, Palaiseau près de l\'ENSAE', '48.71165418969119', '2.209142356072853', 'IUT Orsay', '48.71061586546423', '2.172779602142003', '17:40:00', '18:11:00', '2020-02-25', 3, 'Je fais souvent ce trajet donc pas de problème je peux déposer des gens un peu partout', 0, 1, 0, 1, 1, 0, 0, 0),
(10, 'maxence.noble@gmail.com', 'Ecole Polytechnique, Palaiseau à côté de l\'ENSAE', '48.71159755448673', '2.20921745792527', '23 rue Labrouste Paris', '48.8347623', '2.3088954', '09:46:00', '11:00:00', '2020-02-28', 3, 'Je prends des petites routes', 0, 0, 0, 0, 0, 0, 1, 0),
(11, 'thibaut.vaillant@polytechnique.edu', 'Ecole Polytechnique, Palaiseau', '48.7142971', '2.2112917', 'Complexe Sportif Moulon', '48.70841610000001', '2.157191', '17:40:00', '18:10:00', '2020-02-28', 3, 'RAS', 0, 0, 0, 0, 0, 1, 1, 0),
(12, 'gabriel.velho@polytechnique.edu', 'Ecole Polytechnique, Palaiseau', '48.7142971', '2.2112917', 'Paris XIV', '48.82689985148334', '2.331236802154688', '16:40:00', '18:00:00', '2020-03-03', 1, 'On fera peut-être un petit crochet pour prendre ma mamie :)', 0, 1, 0, 0, 0, 0, 0, 0),
(13, 'gabriel.velho@polytechnique.edu', 'Paris XVI', '48.85642934186985', '2.2864482889710924', 'Ecole Polytechnique, Palaiseau', '48.7142971', '2.2112917', '19:00:00', '20:30:00', '2020-02-24', 4, 'Embouteillages à prévoir', 0, 0, 0, 0, 0, 0, 0, 0),
(14, 'lucie.plazen@polytechnique.edu', 'Rennes', '48.117266', '-1.6777926', 'Ecole Polytechnique, Palaiseau', '48.7142971', '2.2112917', '08:00:00', '12:00:00', '2020-03-06', 2, 'Détour possible n\'importe où', 0, 0, 0, 0, 0, 0, 0, 0),
(15, 'david.admete@polytechnique.edu', 'Ecole Polytechnique, Palaiseau', '48.7142971', '2.2112917', 'Auchan Massy', '48.7266734', '2.2566517', '18:00:00', '19:00:00', '2020-03-01', 5, '---', 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `trajetrecherche`
--

CREATE TABLE `trajetrecherche` (
  `voyagerec` int(11) NOT NULL,
  `demandeur` varchar(128) NOT NULL,
  `depart` varchar(128) NOT NULL,
  `latitude_dep` varchar(128) NOT NULL,
  `longitude_dep` varchar(128) NOT NULL,
  `arrivee` varchar(128) NOT NULL,
  `latitude_arr` varchar(128) NOT NULL,
  `longitude_arr` varchar(128) NOT NULL,
  `debut` time NOT NULL,
  `fin` time NOT NULL,
  `date` date NOT NULL,
  `categorie` varchar(40) NOT NULL,
  `lundi_rec` int(1) NOT NULL,
  `mardi_rec` int(1) NOT NULL,
  `mercredi_rec` int(1) NOT NULL,
  `jeudi_rec` int(1) NOT NULL,
  `vendredi_rec` int(1) NOT NULL,
  `samedi_rec` int(1) NOT NULL,
  `dimanche_rec` int(1) NOT NULL,
  `infosupp` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `trajetrecherche`
--

INSERT INTO `trajetrecherche` (`voyagerec`, `demandeur`, `depart`, `latitude_dep`, `longitude_dep`, `arrivee`, `latitude_arr`, `longitude_arr`, `debut`, `fin`, `date`, `categorie`, `lundi_rec`, `mardi_rec`, `mercredi_rec`, `jeudi_rec`, `vendredi_rec`, `samedi_rec`, `dimanche_rec`, `infosupp`) VALUES
(23, 'leonie.goeb@polytechnique.edu', 'Ecole Polytechnique, Palaiseau', '48.71165418965067', '2.203252225076149', 'Complexe sportif de Moulon', '48.71041708720121', '2.158070413773001', '17:20:00', '17:40:00', '2020-02-28', 'urgent', 0, 0, 0, 0, 0, 0, 0, 'J\'ai un truc super important à faire là-bas !'),
(24, 'thibaut.vaillant@polytechnique.edu', 'Ecole Polytechnique', '37.09024', '-95.712891', 'College d\'Evry', '48.63047240601313', '2.442790977530964', '15:18:00', '17:24:00', '2020-03-02', 'normal', 0, 0, 1, 1, 0, 0, 0, 'Aucun retard toléré !!'),
(25, 'thibaud.pivron@polytechnique.edu', 'Ecole Polytechnique, Palaiseau', '48.71050380349074', '2.2103333473205566', 'Palaiseau ville', '48.7142971', '2.2112917', '15:27:00', '14:39:00', '2020-02-27', 'normal', 0, 0, 0, 0, 0, 0, 0, 'Rien de particulier mais on est là'),
(26, 'thibaut.vaillant@polytechnique.edu', 'IUT Orsay', '48.7028239', '2.174252', 'Ecole Polytechnique, Palaiseau', '48.7142971', '2.2112917', '15:35:00', '16:37:00', '2020-02-29', 'important', 0, 0, 0, 1, 1, 1, 0, 'S\'il vous plaît c\'est urgent ! ;)'),
(27, 'lucie.plazen@polytechnique.edu', 'Ecole Polytechnique, Palaiseau', '48.7142971', '2.2112917', 'Auchan Massy', '48.7266734', '2.2566517', '18:00:00', '19:00:00', '2020-02-28', 'urgent', 1, 1, 1, 1, 1, 1, 1, 'N\'importe quel jour de la semaine me va, mais s\'il vous plaît c\'est urgent !!! UP SVPPPPPPPP'),
(28, 'david.admete@polytechnique.edu', 'Ecole Polytechnique, Palaiseau', '48.7142971', '2.2112917', 'Paris XVIII', '48.8915856', '2.3495999', '16:51:00', '18:00:00', '2020-02-29', 'important', 0, 0, 0, 0, 1, 0, 0, 'Je suis allergiques aux chiens.'),
(30, 'henri.leroy@polytechnique.edu', 'Ecole Polytechnique, Palaiseau', '48.71049220900696', '2.2103602496178665', 'Picadilly Circus', '51.5099695', '-0.1349712', '15:00:00', '21:00:00', '2020-03-02', 'important', 0, 0, 0, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `email` varchar(128) NOT NULL,
  `nom` varchar(64) NOT NULL,
  `prenom` varchar(64) NOT NULL,
  `mdp` varchar(40) NOT NULL,
  `naissance` date NOT NULL,
  `contact` text NOT NULL,
  `descriptif` text DEFAULT NULL,
  `photo` varchar(128) DEFAULT NULL,
  `admin` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`email`, `nom`, `prenom`, `mdp`, `naissance`, `contact`, `descriptif`, `photo`, `admin`) VALUES
('alice@gmail.com', 'Caroll', 'Alice', '72f1df99f773549e7efd352b094cc59549769c41', '1959-01-01', 'Pas vraiment : essayez de crier devant la porte de mon casert en Maunoury au RDC', 'J\'ai une voiture 2 places que j\'utilise pour me rendre chez moi à Paris 04.', '1', 0),
('antoine.bultel@polytechnique.edu', 'Bultel', 'Antoine', '666e94685397ea32d0f94ecc6b73f9075b7b1f01', '1998-08-08', 'par mail svp, sinon mon téléphone : 06.59.33.14.95 !', 'Etudiant à l\'X en 2ème année, je compte passer mon permis bientôt !', '1', 1),
('david.admete@polytechnique.edu', 'Admete', 'David', '6b2830fea571cbbda30a58107b816dd0553b83f6', '1997-08-11', 'Par facebook , je n\'ai plus de portable (mais je suis assez réactif)', 'Master à Centrale Paris, j\'ai un monospace que j\'utilise pour aller à Versailles', '1', 0),
('dominique.a@polytechnique.edu', 'A', 'Dominique', '7de2c62065c3454d482e2697098bc8065b0b2a21', '1970-06-05', 'Par la poste au casert Fayolle', 'Je vis la vie à fond !', '1', 0),
('gabriel.velho@polytechnique.edu', 'Velõ', 'Gabriel', 'b534c55dc194064c4efc288f481d5674acb38f97', '1998-10-03', 'au 0867543232, mais pas par Facebook ou par mail svp !', 'Chercheur en Informatique à l\'INRIA, j\'aimerais bien voyager plus facilement avec des personnes de confiance !', '1', 0),
('henri.leroy@polytechnique.edu', 'Leroy', 'Henri', 'a5b970e5a78bafbc529b0eb0e4cb6815659bd9c2', '1999-10-06', 'Mon adresse e-mail !', 'Je suis élève officier polytechnicien', NULL, 0),
('leonie.goeb@polytechnique.edu', 'Goeb', 'Léonie', '0d4d3d9d2a5bfb25c2f5ca0ad92ddbeeb28c51bc', '1998-03-04', 'Plutôt par Messenger svp', 'Doctorante à l\'X, je n\'ai pas de voiture...', '1', 0),
('lucie.plazen@polytechnique.edu', 'Plazen', 'Lucie', '9ee6402fc287cad7358c44d056fa31421117bae3', '1998-06-03', 'Messenger', 'Etudiante à l\'ENSTA, j\'ai une voiture que j\'utilise assez fréquemment pour aller voir des gens à Centrale.', '1', 0),
('maxence.noble@gmail.com', 'Noble', 'Maxence', '27baea76984fa2677acce73dae373ba9fd90dabb', '1998-12-07', 'Par téléphone : 0647212136 ou par Messenger, pas de problème !', 'étudiant en 2ème année à l\'X, je voyage souvent du côté de Paris. J\'aime bien conduire !', '1', 1),
('olivier.serre@polytechnique.edu', 'Serre', 'Olivier', '2078c6a168190782e639c9a7cf31eb188533356c', '1984-10-04', 'Sur mon téléphone au 0645324356', 'Professeur de Modal, j\'ai une vielle voiture qui roule plutôt bien. Je fais souvent des allers-retours sur le plateau !', '1', 1),
('patrick@gmail.com', 'DE BOB L\'EPONGE', 'Patrick', '641d651450156d6fdae8ce11186976bb3bae52a7', '1989-08-23', 'Je vis sous la mer', 'Rien je suis arrivé ici par erreur', '1', 0),
('thibaud.pivron@polytechnique.edu', 'Pivron', 'Thibaud', 'e895777e50a6f6aa6a14f1935f5ff8e6a1fb018c', '1999-07-02', 'Mail', 'étudiant à Polytechnique', NULL, 0),
('thibaut.vaillant@polytechnique.edu', 'Vaillant', 'Thibaut', 'e031f825486d7185d61a3c2f0d7c94870b09aca5', '1998-03-02', '0657435265', 'Pas grand chose de spécial, je vis au jour le jour...', '1', 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `etapes`
--
ALTER TABLE `etapes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `voyage` (`voyage`);

--
-- Index pour la table `trajetaccepte`
--
ALTER TABLE `trajetaccepte`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client` (`client`);

--
-- Index pour la table `trajetpropose`
--
ALTER TABLE `trajetpropose`
  ADD PRIMARY KEY (`voyagepro`),
  ADD KEY `conducteur` (`conducteur`);

--
-- Index pour la table `trajetrecherche`
--
ALTER TABLE `trajetrecherche`
  ADD PRIMARY KEY (`voyagerec`),
  ADD KEY `demandeur` (`demandeur`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `etapes`
--
ALTER TABLE `etapes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `trajetaccepte`
--
ALTER TABLE `trajetaccepte`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT pour la table `trajetpropose`
--
ALTER TABLE `trajetpropose`
  MODIFY `voyagepro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `trajetrecherche`
--
ALTER TABLE `trajetrecherche`
  MODIFY `voyagerec` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `etapes`
--
ALTER TABLE `etapes`
  ADD CONSTRAINT `FK_etapes` FOREIGN KEY (`voyage`) REFERENCES `trajetpropose` (`voyagepro`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_trajetpropose_id` FOREIGN KEY (`voyage`) REFERENCES `trajetpropose` (`voyagepro`);

--
-- Contraintes pour la table `trajetaccepte`
--
ALTER TABLE `trajetaccepte`
  ADD CONSTRAINT `FK_accepte` FOREIGN KEY (`client`) REFERENCES `utilisateurs` (`email`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_trajetaccepte_client` FOREIGN KEY (`client`) REFERENCES `utilisateurs` (`email`);

--
-- Contraintes pour la table `trajetpropose`
--
ALTER TABLE `trajetpropose`
  ADD CONSTRAINT `FK_pro` FOREIGN KEY (`conducteur`) REFERENCES `utilisateurs` (`email`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_trajetpropose_client` FOREIGN KEY (`conducteur`) REFERENCES `utilisateurs` (`email`);

--
-- Contraintes pour la table `trajetrecherche`
--
ALTER TABLE `trajetrecherche`
  ADD CONSTRAINT `FK_rec` FOREIGN KEY (`demandeur`) REFERENCES `utilisateurs` (`email`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_trajetrecherche_client` FOREIGN KEY (`demandeur`) REFERENCES `utilisateurs` (`email`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
